export default [
  {
    path: "/messages",
    redirect: "/messages/questions",
    name: "email",
    meta: {
      rule: "A-CE-01",
      authRequired: true
    }
  },
  {
    path: "/messages/:filter",
    name: "page-list-view-messages",
    component: () => import("@/views/pages/messages/Messages.vue"),
    meta: {
      breadcrumb: [{ title: "Mensajes", active: true }],
      rule: "A-CE-01",
      authRequired: true
    }
  }
];
