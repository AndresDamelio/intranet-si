export default [
  {
    path: "/pages/faq",
    name: "page-faq",
    component: () => import("@/views/pages/Faq.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Pages" },
        { title: "FAQ", active: true }
      ],
      pageTitle: "FAQ",
      rule: "editor"
    }
  }
];
