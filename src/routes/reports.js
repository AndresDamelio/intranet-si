export default [
  {
    path: "/reports",
    name: "page-reports",
    component: () => import("@/views/pages/reports/Reports.vue"),
    meta: {
      breadcrumb: [{ title: "Reportes estadísticos", active: true }],
      rule: "F-RE-01",
      authRequired: true
    }
  }
];
