export default [
  {
    path: "/services",
    name: "page-services",
    component: () => import("@/views/pages/transactions/service/ListView.vue"),
    meta: {
      breadcrumb: [{ title: "Servicios", active: true }],
      rule: "F-SE-02",
      authRequired: true
    }
  },
  {
    path: "/service/create",
    name: "page-create-service",
    component: () => import("@/views/pages/transactions/service/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Servicios", url: "/services" },
        { title: "Crear", active: true }
      ],
      rule: "F-SE-02",
      authRequired: true
    }
  },
  {
    path: "/service/:id",
    name: "page-edit-service",
    component: () => import("@/views/pages/transactions/service/ViewEdit.vue"),
    meta: {
      breadcrumb: [
        { title: "Servicios", url: "/services" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "F-SE-02",
      authRequired: true
    }
  },
  {
    path: "/my-services",
    name: "page-my-services",
    component: () => import("@/views/pages/transactions/service/ViewEdit.vue"),
    meta: {
      breadcrumb: [{ title: "Mis Servicios", active: true }],
      rule: "F-SE-01",
      authRequired: true
    }
  }
];
