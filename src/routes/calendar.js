export default [
  {
    path: "/calendar",
    name: "page-view-calendar",
    component: () => import("@/views/pages/calendar/Calendar.vue"),
    meta: {
      breadcrumb: [{ title: "Agenda", active: true }],
      authRequired: true
    }
  }
];
