export default [
  {
    path: "/notification-types",
    name: "page-notification-type",
    component: () =>
      import(
        "@/views/pages/configuration/basic-data/notification-type/ListView.vue"
      ),
    meta: {
      breadcrumb: [{ title: "Tipos de notificación", active: true }],
      rule: "A-DB-15",
      authRequired: true
    }
  },
  {
    path: "/notification-type/create",
    name: "page-create-notification-type",
    component: () =>
      import(
        "@/views/pages/configuration/basic-data/notification-type/Create.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Tipos de notificación", url: "/notification-types" },
        { title: "Crear", active: true }
      ],
      rule: "A-DB-15",
      authRequired: true
    }
  },
  {
    path: "/notification-type/:id",
    name: "page-edit-notification-type",
    component: () =>
      import(
        "@/views/pages/configuration/basic-data/notification-type/ViewEdit.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Tipos de notificación", url: "/notification-types" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "A-DB-15",
      authRequired: true
    }
  }
];
