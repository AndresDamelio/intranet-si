export default [
  {
    path: "/positions",
    name: "page-position",
    component: () =>
      import("@/views/pages/configuration/basic-data/position/ListView.vue"),
    meta: {
      breadcrumb: [{ title: "Cargos", active: true }],
      pageTitle: "Cargos",
      rule: "A-DB-02",
      authRequired: true
    }
  },
  {
    path: "/position/create",
    name: "page-create-position",
    component: () =>
      import("@/views/pages/configuration/basic-data/position/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Cargos", url: "/positions" },
        { title: "Crear cargo", active: true }
      ],
      pageTitle: "Crear cargo",
      rule: "A-DB-02",
      authRequired: true
    }
  },
  {
    path: "/position/:id",
    name: "page-edit-position",
    component: () =>
      import("@/views/pages/configuration/basic-data/position/ViewEdit.vue"),
    meta: {
      breadcrumb: [
        { title: "Cargos", url: "/positions" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      pageTitle: "Cargo",
      rule: "A-DB-02",
      authRequired: true
    }
  }
];
