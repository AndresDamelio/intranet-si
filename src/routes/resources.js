export default [
  {
    path: "/resources",
    name: "page-resource",
    component: () =>
      import("@/views/pages/configuration/basic-data/resource/ListView.vue"),
    meta: {
      breadcrumb: [{ title: "Recursos", active: true }],
      rule: "A-DB-11",
      authRequired: true
    }
  },
  {
    path: "/resource/create",
    name: "page-create-resource",
    component: () =>
      import("@/views/pages/configuration/basic-data/resource/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Recurso", url: "/resources" },
        { title: "Crear", active: true }
      ],
      rule: "A-DB-11",
      authRequired: true
    }
  },
  {
    path: "/resource/:id",
    name: "page-edit-resource",
    component: () =>
      import("@/views/pages/configuration/basic-data/resource/ViewEdit.vue"),
    meta: {
      breadcrumb: [
        { title: "Recurso", url: "/resources" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "A-DB-11",
      authRequired: true
    }
  }
];
