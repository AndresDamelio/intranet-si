export default [
  {
    path: "/pathologies",
    name: "page-list-view-pathologies",
    component: () =>
      import("@/views/pages/configuration/basic-data/pathology/ListView.vue"),
    meta: {
      breadcrumb: [{ title: "Patologias", active: true }],
      rule: "A-DB-08",
      authRequired: true
    }
  },
  {
    path: "/pathology/create",
    name: "page-create-pathology",
    component: () =>
      import("@/views/pages/configuration/basic-data/pathology/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Patologias", url: "/pathologies" },
        { title: "Crear", active: true }
      ],
      rule: "A-DB-08",
      authRequired: true
    }
  },
  {
    path: "/pathology/:id",
    name: "page-edit-pathology",
    component: () =>
      import("@/views/pages/configuration/basic-data/pathology/ViewEdit.vue"),
    meta: {
      breadcrumb: [
        { title: "Patologias", url: "/pathologies" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "A-DB-08",
      authRequired: true
    }
  }
];
