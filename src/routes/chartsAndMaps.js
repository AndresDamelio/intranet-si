export default [
  {
    path: "/charts-and-maps/charts/apex-charts",
    name: "extra-component-charts-apex-charts",
    component: () =>
      import("@/views/charts-and-maps/charts/apex-charts/ApexCharts.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Charts & Maps" },
        { title: "Apex Charts", active: true }
      ],
      pageTitle: "Apex Charts",
      rule: "editor"
    }
  },
  {
    path: "/charts-and-maps/charts/chartjs",
    name: "extra-component-charts-chartjs",
    component: () =>
      import("@/views/charts-and-maps/charts/chartjs/Chartjs.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Charts & Maps" },
        { title: "chartjs", active: true }
      ],
      pageTitle: "chartjs",
      rule: "editor"
    }
  },
  {
    path: "/charts-and-maps/charts/echarts",
    name: "extra-component-charts-echarts",
    component: () =>
      import("@/views/charts-and-maps/charts/echarts/Echarts.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Charts & Maps" },
        { title: "echarts", active: true }
      ],
      pageTitle: "echarts",
      rule: "editor"
    }
  },
];
