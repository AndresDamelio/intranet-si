export default [
  {
    path: "/specialities",
    name: "page-speciality",
    component: () =>
      import("@/views/pages/configuration/basic-data/speciality/ListView.vue"),
    meta: {
      breadcrumb: [{ title: "Especialidades", active: true }],
      rule: "A-DB-04",
      authRequired: true
    }
  },
  {
    path: "/speciality/create",
    name: "page-create-speciality",
    component: () =>
      import("@/views/pages/configuration/basic-data/speciality/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Especialidades", url: "/specialities" },
        { title: "Crear especialidad", active: true }
      ],
      rule: "A-DB-04",
      authRequired: true
    }
  },
  {
    path: "/speciality/:id",
    name: "page-edit-speciality",
    component: () =>
      import("@/views/pages/configuration/basic-data/speciality/ViewEdit.vue"),
    meta: {
      breadcrumb: [
        { title: "Especialidades", url: "/specialities" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "A-DB-04",
      authRequired: true
    }
  }
];
