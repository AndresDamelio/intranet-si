export default [
  {
    path: "/activities",
    name: "page-activity",
    component: () => import("@/views/pages/transactions/activity/ListView.vue"),
    meta: {
      breadcrumb: [{ title: "Actividades", active: true }],
      rule: "F-AC-02",
      authRequired: true
    }
  },
  {
    path: "/activity/create",
    name: "page-create-activity",
    component: () => import("@/views/pages/transactions/activity/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Actividades", url: "/activities" },
        { title: "Crear", active: true }
      ],
      rule: "F-AC-02",
      authRequired: true
    }
  },
  {
    path: "/activity/:id",
    name: "page-edit-activity",
    component: () => import("@/views/pages/transactions/activity/ViewEdit.vue"),
    meta: {
      breadcrumb: [
        { title: "Actividades", url: "/activities" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "F-AC-02",
      authRequired: true
    }
  },
  {
    path: "/my-activities",
    name: "page-my-activities",
    component: () => import("@/views/pages/transactions/activity/ViewEdit.vue"),
    meta: {
      breadcrumb: [
        { title: "Actividades", url: "/activities" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "F-AC-01",
      authRequired: true
    }
  }
];
