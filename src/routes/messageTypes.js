export default [
  {
    path: "/message-types",
    name: "page-message-type",
    component: () =>
      import(
        "@/views/pages/configuration/basic-data/message-type/ListView.vue"
      ),
    meta: {
      breadcrumb: [{ title: "Tipos de mensaje", active: true }],
      rule: "A-DB-14",
      authRequired: true
    }
  },
  {
    path: "/message-type/create",
    name: "page-create-message-type",
    component: () =>
      import("@/views/pages/configuration/basic-data/message-type/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Tipos de mensaje", url: "/message-types" },
        { title: "Crear", active: true }
      ],
      rule: "A-DB-14",
      authRequired: true
    }
  },
  {
    path: "/message-type/:id",
    name: "page-edit-message-type",
    component: () =>
      import(
        "@/views/pages/configuration/basic-data/message-type/ViewEdit.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Tipos de mensaje", url: "/message-types" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "A-DB-14",
      authRequired: true
    }
  }
];
