export default [
  {
    path: "/service-types",
    name: "page-service-type",
    component: () =>
      import(
        "@/views/pages/configuration/basic-data/service-type/ListView.vue"
      ),
    meta: {
      breadcrumb: [{ title: "Tipos de servicio", active: true }],
      pageTitle: "Tipos de servicio",
      rule: "A-DB-17",
      authRequired: true
    }
  },
  {
    path: "/service-type/create",
    name: "page-create-service-type",
    component: () =>
      import("@/views/pages/configuration/basic-data/service-type/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Tipos de servicio", url: "/service-types" },
        { title: "Crear tipo de servicio", active: true }
      ],
      pageTitle: "Crear tipo de servicio",
      rule: "A-DB-17",
      authRequired: true
    }
  },
  {
    path: "/service-type/:id",
    name: "page-edit-service-type",
    component: () =>
      import(
        "@/views/pages/configuration/basic-data/service-type/ViewEdit.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Tipos de servicio", url: "/service-types" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      pageTitle: "Tipo de servicio",
      rule: "A-DB-17",
      authRequired: true
    }
  }
];
