export default [
    {
      path: "/posts",
      name: "page-list-view-posts",
      component: () =>
        import("@/views/pages/configuration/web-movil/post/ListView.vue"),
      meta: {
        breadcrumb: [{ title: "Publicaciones", active: true }],
        rule: "A-WM-04",
        authRequired: true
      }
    },
    {
      path: "/post/create",
      name: "page-create-post",
      component: () =>
        import("@/views/pages/configuration/web-movil/post/Create.vue"),
      meta: {
        breadcrumb: [
          { title: "Publicaciones", url: "/posts" },
          { title: "Crear", active: true }
        ],
        rule: "A-WM-04",
        authRequired: true
      }
    },
    {
      path: "/post/:id",
      name: "page-edit-post",
      component: () =>
        import("@/views/pages/configuration/web-movil/post/ViewEdit.vue"),
      meta: {
        breadcrumb: [
          { title: "Publicaciones", url: "/posts" },
          { title: "Detalle", active: true, dynamic: true }
        ],
        rule: "A-WM-04",
        authRequired: true
      }
    }
  ];