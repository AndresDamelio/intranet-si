export default [
  /* Schedules */
  {
    path: "/schedules",
    name: "page-schedule",
    component: () =>
      import("@/views/pages/configuration/basic-data/schedule/ListView.vue"),
    meta: {
      breadcrumb: [{ title: "Turnos", active: true }],
      rule: "A-DB-05",
      authRequired: true
    }
  },
  {
    path: "/schedule/create",
    name: "page-create-schedule",
    component: () =>
      import("@/views/pages/configuration/basic-data/schedule/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Turnos", url: "/schedules" },
        { title: "Crear", active: true }
      ],
      rule: "A-DB-05",
      authRequired: true
    }
  },
  {
    path: "/schedule/:id",
    name: "page-edit-schedule",
    component: () =>
      import("@/views/pages/configuration/basic-data/schedule/ViewEdit.vue"),
    meta: {
      breadcrumb: [
        { title: "Turnos", url: "/schedules" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "A-DB-05",
      authRequired: true
    }
  },
];
