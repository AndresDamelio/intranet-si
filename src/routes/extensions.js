export default [
  {
    path: "/extensions/select",
    name: "extra-component-select",
    component: () =>
      import("@/views/components/extra-components/select/Select.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Extra Components" },
        { title: "Select", active: true }
      ],
      pageTitle: "Select",
      rule: "editor"
    }
  },
  {
    path: "/extensions/quill-editor",
    name: "extra-component-quill-editor",
    component: () =>
      import(
        "@/views/components/extra-components/quill-editor/QuillEditor.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Extra Components" },
        { title: "Quill Editor", active: true }
      ],
      pageTitle: "Quill Editor",
      rule: "editor"
    }
  },
  {
    path: "/extensions/datepicker",
    name: "extra-component-datepicker",
    component: () =>
      import("@/views/components/extra-components/datepicker/Datepicker.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Extra Components" },
        { title: "Datepicker", active: true }
      ],
      pageTitle: "Datepicker",
      rule: "editor"
    }
  },
  {
    path: "/extensions/datetime-picker",
    name: "extra-component-datetime-picker",
    component: () =>
      import(
        "@/views/components/extra-components/datetime-picker/DatetimePicker.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Extra Components" },
        { title: "Datetime Picker", active: true }
      ],
      pageTitle: "Datetime Picker",
      rule: "editor"
    }
  },
  {
    path: "/extensions/access-control",
    name: "extra-component-access-control",
    component: () =>
      import(
        "@/views/components/extra-components/access-control/AccessControl.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Extensions" },
        { title: "Access Control", active: true }
      ],
      pageTitle: "Access Control",
      rule: "editor"
    }
  },
  {
    path: "/extensions/i18n",
    name: "extra-component-i18n",
    component: () => import("@/views/components/extra-components/I18n.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Extensions" },
        { title: "I18n", active: true }
      ],
      pageTitle: "I18n",
      rule: "editor"
    }
  },
  {
    path: "/extensions/clipboard",
    name: "extra-component-clipboard",
    component: () =>
      import("@/views/components/extra-components/clipboard/Clipboard.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Extensions" },
        { title: "Clipboard", active: true }
      ],
      pageTitle: "Clipboard",
      rule: "editor"
    }
  },
  {
    path: "/extensions/context-menu",
    name: "extra-component-context-menu",
    component: () =>
      import(
        "@/views/components/extra-components/context-menu/ContextMenu.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Extensions" },
        { title: "Context Menu", active: true }
      ],
      pageTitle: "Context Menu",
      rule: "editor"
    }
  },
  {
    path: "/extensions/star-ratings",
    name: "extra-component-star-ratings",
    component: () =>
      import(
        "@/views/components/extra-components/star-ratings/StarRatings.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Extensions" },
        { title: "Star Ratings", active: true }
      ],
      pageTitle: "Star Ratings",
      rule: "editor"
    }
  },
  {
    path: "/extensions/autocomplete",
    name: "extra-component-autocomplete",
    component: () =>
      import(
        "@/views/components/extra-components/autocomplete/Autocomplete.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Extensions" },
        { title: "Autocomplete", active: true }
      ],
      pageTitle: "Autocomplete",
      rule: "editor"
    }
  },
  {
    path: "/extensions/tree",
    name: "extra-component-tree",
    component: () =>
      import("@/views/components/extra-components/tree/Tree.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Extensions" },
        { title: "Tree", active: true }
      ],
      pageTitle: "Tree",
      rule: "editor"
    }
  },
  {
    path: "/import-export/import",
    name: "import-excel",
    component: () =>
      import("@/views/components/extra-components/import-export/Import.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Extensions" },
        { title: "Import/Export" },
        { title: "Import", active: true }
      ],
      pageTitle: "Import Excel",
      rule: "editor"
    }
  },
  {
    path: "/import-export/export",
    name: "export-excel",
    component: () =>
      import("@/views/components/extra-components/import-export/Export.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Extensions" },
        { title: "Import/Export" },
        { title: "Export", active: true }
      ],
      pageTitle: "Export Excel",
      rule: "editor"
    }
  },
  {
    path: "/import-export/export-selected",
    name: "export-excel-selected",
    component: () =>
      import(
        "@/views/components/extra-components/import-export/ExportSelected.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Extensions" },
        { title: "Import/Export" },
        { title: "Export Selected", active: true }
      ],
      pageTitle: "Export Excel",
      rule: "editor"
    }
  }
];
