export default [
  {
    path: "/ui-elements/data-list/list-view",
    name: "data-list-list-view",
    component: () =>
      import("@/views/ui-elements/data-list/list-view/DataListListView.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Data List" },
        { title: "List View", active: true }
      ],
      pageTitle: "List View",
      rule: "editor"
    }
  },
  {
    path: "/ui-elements/data-list/thumb-view",
    name: "data-list-thumb-view",
    component: () =>
      import("@/views/ui-elements/data-list/thumb-view/DataListThumbView.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Data List" },
        { title: "Thumb View", active: true }
      ],
      pageTitle: "Thumb View",
      rule: "editor"
    }
  },
  {
    path: "/ui-elements/card/basic",
    name: "basic-cards",
    component: () => import("@/views/ui-elements/card/CardBasic.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Card" },
        { title: "Basic Cards", active: true }
      ],
      pageTitle: "Basic Cards",
      rule: "editor"
    }
  },
  {
    path: "/ui-elements/card/statistics",
    name: "statistics-cards",
    component: () => import("@/views/ui-elements/card/CardStatistics.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Card" },
        { title: "Statistics Cards", active: true }
      ],
      pageTitle: "Statistics Card",
      rule: "editor"
    }
  },
  {
    path: "/ui-elements/card/analytics",
    name: "analytics-cards",
    component: () => import("@/views/ui-elements/card/CardAnalytics.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Card" },
        { title: "Analytics Card", active: true }
      ],
      pageTitle: "Analytics Card",
      rule: "editor"
    }
  },
  {
    path: "/ui-elements/card/card-actions",
    name: "card-actions",
    component: () => import("@/views/ui-elements/card/CardActions.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Card" },
        { title: "Card Actions", active: true }
      ],
      pageTitle: "Card Actions",
      rule: "editor"
    }
  },
  {
    path: "/ui-elements/card/card-colors",
    name: "card-colors",
    component: () => import("@/views/ui-elements/card/CardColors.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Card" },
        { title: "Card Colors", active: true }
      ],
      pageTitle: "Card Colors",
      rule: "editor"
    }
  },
  {
    path: "/ui-elements/ag-grid-table",
    name: "ag-grid-table",
    component: () =>
      import("@/views/ui-elements/ag-grid-table/AgGridTable.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "agGrid Table", active: true }
      ],
      pageTitle: "agGrid Table",
      rule: "editor"
    }
  }
];
