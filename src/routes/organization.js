export default [
  {
    path: "/organization/settings",
    name: "page-organization-settings",
    component: () =>
      import(
        "@/views/pages/configuration/organization/OrganizationSettings.vue"
      ),
    meta: {
      breadcrumb: [{ title: "Organización", active: true }],
      rule: "A-OR-01",
      authRequired: true
    }
  }
];
