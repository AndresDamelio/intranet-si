export default [
  {
    path: "/backup",
    name: "page-database",
    component: () => import("@/views/pages/database/ListBackup.vue"),
    meta: {
      breadcrumb: [{ title: "Respaldos", active: true }],
      rule: "A-BD-01",
      authRequired: true
    }
  },
  {
    path: "/restore",
    name: "page-restore",
    component: () => import("@/views/pages/database/Restore.vue"),
    meta: {
      breadcrumb: [{ title: "Restaurar", active: true }],
      rule: "A-BD-02",
      authRequired: true
    }
  },
  {
    path: "/clean",
    name: "page-clean",
    component: () => import("@/views/pages/database/Clean.vue"),
    meta: {
      breadcrumb: [{ title: "Limpiar base de datos", active: true }],
      rule: "A-BD-03",
      authRequired: true
    }
  }
];
