export default [
  {
    path: "/reason-rejections",
    name: "page-reason-rejection",
    component: () =>
      import(
        "@/views/pages/configuration/basic-data/reason-rejection/ListView.vue"
      ),
    meta: {
      breadcrumb: [{ title: "Motivos de rechazo", active: true }],
      pageTitle: "Motivos de rechazo",
      rule: "A-DB-07",
      authRequired: true
    }
  },
  {
    path: "/reason-rejection/create",
    name: "page-create-reason-rejection",
    component: () =>
      import(
        "@/views/pages/configuration/basic-data/reason-rejection/Create.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Motivos de rechazo", url: "/reason-rejections" },
        { title: "Crear motivo de rechazo", active: true }
      ],
      pageTitle: "Crear motivo de rechazo",
      rule: "A-DB-07",
      authRequired: true
    }
  },
  {
    path: "/reason-rejection/:id",
    name: "page-edit-reason-rejection",
    component: () =>
      import(
        "@/views/pages/configuration/basic-data/reason-rejection/ViewEdit.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Motivo de rechazo", url: "/reason-rejections" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      pageTitle: "Motivo de rechazo",
      rule: "A-DB-07",
      authRequired: true
    }
  }
];
