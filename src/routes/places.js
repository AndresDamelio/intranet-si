export default [
  {
    path: "/places",
    name: "page-place",
    component: () =>
      import("@/views/pages/configuration/basic-data/place/ListView.vue"),
    meta: {
      breadcrumb: [{ title: "Lugares", active: true }],
      rule: "A-DB-06",
      authRequired: true
    }
  },
  {
    path: "/place/create",
    name: "page-create-place",
    component: () =>
      import("@/views/pages/configuration/basic-data/place/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Lugares", url: "/places" },
        { title: "Crear", active: true }
      ],
      rule: "A-DB-06",
      authRequired: true
    }
  },
  {
    path: "/place/:id",
    name: "page-edit-place",
    component: () =>
      import("@/views/pages/configuration/basic-data/place/ViewEdit.vue"),
    meta: {
      breadcrumb: [
        { title: "Lugares", url: "/places" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "A-DB-06",
      authRequired: true
    }
  }
];
