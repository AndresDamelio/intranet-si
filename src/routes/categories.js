export default [
  {
    path: "/categories",
    name: "page-category",
    component: () =>
      import("@/views/pages/configuration/basic-data/category/ListView.vue"),
    meta: {
      breadcrumb: [{ title: "Categorías", active: true }],
      rule: "A-DB-03",
      authRequired: true
    }
  },
  {
    path: "/category/create",
    name: "page-create-category",
    component: () =>
      import("@/views/pages/configuration/basic-data/category/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Categorías", url: "/categories" },
        { title: "Crear", active: true }
      ],
      rule: "A-DB-03",
      authRequired: true
    }
  },
  {
    path: "/category/:id",
    name: "page-edit-category",
    component: () =>
      import("@/views/pages/configuration/basic-data/category/ViewEdit.vue"),
    meta: {
      breadcrumb: [
        { title: "Categorías", url: "/categories" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "A-DB-03",
      authRequired: true
    }
  }
];
