export default [
  {
    path: "/resource-types",
    name: "page-resource-type",
    component: () =>
      import(
        "@/views/pages/configuration/basic-data/resource-type/ListView.vue"
      ),
    meta: {
      breadcrumb: [{ title: "Tipos de recurso", active: true }],
      pageTitle: "Tipos de recurso",
      rule: "A-DB-16",
      authRequired: true
    }
  },
  {
    path: "/resource-type/create",
    name: "page-create-resource-type",
    component: () =>
      import("@/views/pages/configuration/basic-data/resource-type/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Tipos de recurso", url: "/resource-types" },
        { title: "Crear tipo de recurso", active: true }
      ],
      pageTitle: "Crear tipo de recurso",
      rule: "A-DB-16",
      authRequired: true
    }
  },
  {
    path: "/resource-type/:id",
    name: "page-edit-resource-type",
    component: () =>
      import(
        "@/views/pages/configuration/basic-data/resource-type/ViewEdit.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Tipos de recurso", url: "/resource-types" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      pageTitle: "Tipo de recurso",
      rule: "A-DB-16",
      authRequired: true
    }
  }
];
