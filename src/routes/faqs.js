export default [
  {
    path: "/faqs",
    name: "page-list-view-faqs",
    component: () =>
      import("@/views/pages/configuration/web-movil/faq/ListView.vue"),
    meta: {
      breadcrumb: [{ title: "Preguntas frecuentes", active: true }],
      rule: "A-WM-01",
      authRequired: true
    }
  },
  {
    path: "/faq/create",
    name: "page-create-faq",
    component: () =>
      import("@/views/pages/configuration/web-movil/faq/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Preguntas frecuentes", url: "/faqs" },
        { title: "Crear", active: true }
      ],
      rule: "A-WM-01",
      authRequired: true
    }
  },
  {
    path: "/faq/:id",
    name: "page-edit-faq",
    component: () =>
      import("@/views/pages/configuration/web-movil/faq/ViewEdit.vue"),
    meta: {
      breadcrumb: [
        { title: "Preguntas frecuentes", url: "/faqs" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "A-WM-01",
      authRequired: true
    }
  }
];
