export default [
  {
    path: "/activity-configs",
    name: "page-activity-setting",
    component: () =>
      import(
        "@/views/pages/configuration/settings/activity-setting/ListView.vue"
      ),
    meta: {
      breadcrumb: [{ title: "Configuraciones de actividad", active: true }],
      rule: "A-CF-01",
      authRequired: true
    }
  },
  {
    path: "/activity-config/create",
    name: "page-create-activity-config",
    component: () =>
      import(
        "@/views/pages/configuration/settings/activity-setting/Create.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Configuraciones de actividad", url: "/activity-configs" },
        { title: "Crear", active: true }
      ],
      rule: "A-CF-01",
      authRequired: true
    }
  },
  {
    path: "/activity-config/:id",
    name: "page-config-view",
    component: () =>
      import(
        "@/views/pages/configuration/settings/activity-setting/View.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Configuraciones de actividad", url: "/activity-configs" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "A-CF-01",
      authRequired: true
    }
  },
  {
    path: "/activity-config/edit/:id",
    name: "page-edit-activity-setting",
    component: () =>
      import(
        "@/views/pages/configuration/settings/activity-setting/Edit.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Configuraciones de actividad", url: "/activity-configs" },
        { title: "Editar", active: true, dynamic: true }
      ],
      rule: "A-CF-01",
      authRequired: true
    }
  }
];
