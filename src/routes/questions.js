export default [
  {
    path: "/questions",
    name: "page-question",
    component: () =>
      import("@/views/pages/configuration/basic-data/question/ListView.vue"),
    meta: {
      breadcrumb: [{ title: "Preguntas", active: true }],
      rule: "A-DB-10",
      authRequired: true
    }
  },
  {
    path: "/question/create",
    name: "page-create-question",
    component: () =>
      import("@/views/pages/configuration/basic-data/question/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Preguntas", url: "/questions" },
        { title: "Crear", active: true }
      ],
      rule: "A-DB-10",
      authRequired: true
    }
  },
  {
    path: "/question/:id",
    name: "page-edit-question",
    component: () =>
      import("@/views/pages/configuration/basic-data/question/ViewEdit.vue"),
    meta: {
      breadcrumb: [
        { title: "Preguntas", url: "/questions" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "A-DB-10",
      authRequired: true
    }
  }
];
