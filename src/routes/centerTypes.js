export default [
  {
    path: "/center-types",
    name: "page-list-view-center-types",
    component: () =>
      import("@/views/pages/configuration/basic-data/center-type/ListView.vue"),
    meta: {
      breadcrumb: [{ title: "Tipos de centro", active: true }],
      rule: "A-DB-13",
      authRequired: true
    }
  },
  {
    path: "/center-type/create",
    name: "page-create-center-type",
    component: () =>
      import("@/views/pages/configuration/basic-data/center-type/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Tipos de centro", url: "/center-types" },
        { title: "Crear", active: true }
      ],
      rule: "A-DB-13",
      authRequired: true
    }
  },
  {
    path: "/center-type/:id",
    name: "page-edit-center-type",
    component: () =>
      import("@/views/pages/configuration/basic-data/center-type/ViewEdit.vue"),
    meta: {
      breadcrumb: [
        { title: "Tipos de centro", url: "/center-types" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "A-DB-13",
      authRequired: true
    }
  }
];
