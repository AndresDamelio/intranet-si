export default [
  {
    path: "/components/alert",
    name: "component-alert",
    component: () => import("@/views/components/vuesax/alert/Alert.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Components" },
        { title: "Alert", active: true }
      ],
      pageTitle: "Alert",
      rule: "editor"
    }
  },
  {
    path: "/components/avatar",
    name: "component-avatar",
    component: () => import("@/views/components/vuesax/avatar/Avatar.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Components" },
        { title: "Avatar", active: true }
      ],
      pageTitle: "Avatar",
      rule: "editor"
    }
  },
  {
    path: "/components/button",
    name: "component-button",
    component: () => import("@/views/components/vuesax/button/Button.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Components" },
        { title: "Button", active: true }
      ],
      pageTitle: "Button",
      rule: "editor"
    }
  },
  {
    path: "/components/button-group",
    name: "component-button-group",
    component: () =>
      import("@/views/components/vuesax/button-group/ButtonGroup.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Components" },
        { title: "Button Group", active: true }
      ],
      pageTitle: "Button Group",
      rule: "editor"
    }
  },
  {
    path: "/components/chip",
    name: "component-chip",
    component: () => import("@/views/components/vuesax/chip/Chip.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Components" },
        { title: "Chip", active: true }
      ],
      pageTitle: "Chip",
      rule: "editor"
    }
  },
  {
    path: "/components/collapse",
    name: "component-collapse",
    component: () => import("@/views/components/vuesax/collapse/Collapse.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Components" },
        { title: "Collapse", active: true }
      ],
      pageTitle: "Collapse",
      rule: "editor"
    }
  },
  {
    path: "/components/dialogs",
    name: "component-dialog",
    component: () => import("@/views/components/vuesax/dialogs/Dialogs.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Components" },
        { title: "Dialogs", active: true }
      ],
      pageTitle: "Dialogs",
      rule: "editor"
    }
  },
  {
    path: "/components/divider",
    name: "component-divider",
    component: () => import("@/views/components/vuesax/divider/Divider.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Components" },
        { title: "Divider", active: true }
      ],
      pageTitle: "Divider",
      rule: "editor"
    }
  },
  {
    path: "/components/dropdown",
    name: "component-drop-down",
    component: () => import("@/views/components/vuesax/dropdown/Dropdown.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Components" },
        { title: "Dropdown", active: true }
      ],
      pageTitle: "Dropdown",
      rule: "editor"
    }
  },
  {
    path: "/components/list",
    name: "component-list",
    component: () => import("@/views/components/vuesax/list/List.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Components" },
        { title: "List", active: true }
      ],
      pageTitle: "List",
      rule: "editor"
    }
  },
  {
    path: "/components/loading",
    name: "component-loading",
    component: () => import("@/views/components/vuesax/loading/Loading.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Components" },
        { title: "Loading", active: true }
      ],
      pageTitle: "Loading",
      rule: "editor"
    }
  },
  {
    path: "/components/notifications",
    name: "component-notifications",
    component: () =>
      import("@/views/components/vuesax/notifications/Notifications.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Components" },
        { title: "Notifications", active: true }
      ],
      pageTitle: "Notifications",
      rule: "editor"
    }
  },
  {
    path: "/components/popup",
    name: "component-popup",
    component: () => import("@/views/components/vuesax/popup/Popup.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Components" },
        { title: "Popup", active: true }
      ],
      pageTitle: "Popup",
      rule: "editor"
    }
  },
  {
    path: "/components/progress",
    name: "component-progress",
    component: () => import("@/views/components/vuesax/progress/Progress.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Components" },
        { title: "Progress", active: true }
      ],
      pageTitle: "Progress",
      rule: "editor"
    }
  },
  {
    path: "/components/slider",
    name: "component-slider",
    component: () => import("@/views/components/vuesax/slider/Slider.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Components" },
        { title: "Slider", active: true }
      ],
      pageTitle: "Slider",
      rule: "editor"
    }
  },
  {
    path: "/components/tabs",
    name: "component-tabs",
    component: () => import("@/views/components/vuesax/tabs/Tabs.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Components" },
        { title: "Tabs", active: true }
      ],
      pageTitle: "Tabs",
      rule: "editor"
    }
  },
  {
    path: "/components/tooltip",
    name: "component-tooltip",
    component: () => import("@/views/components/vuesax/tooltip/Tooltip.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "Components" },
        { title: "Tooltip", active: true }
      ],
      pageTitle: "Tooltip",
      rule: "editor"
    }
  },
];
