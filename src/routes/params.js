export default [
  {
    path: "/params",
    name: "page-param",
    component: () =>
      import("@/views/pages/configuration/basic-data/param/ListView.vue"),
    meta: {
      breadcrumb: [{ title: "Parámetros", active: true }],
      rule: "A-DB-09",
      authRequired: true
    }
  },
  {
    path: "/param/create",
    name: "page-create-param",
    component: () =>
      import("@/views/pages/configuration/basic-data/param/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Parámetros", url: "/params" },
        { title: "Crear", active: true }
      ],
      rule: "A-DB-09",
      authRequired: true
    }
  },
  {
    path: "/param/:id",
    name: "page-edit-param",
    component: () =>
      import("@/views/pages/configuration/basic-data/param/ViewEdit.vue"),
    meta: {
      breadcrumb: [
        { title: "Parámetros", url: "/params" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "A-DB-09",
      authRequired: true
    }
  }
];
