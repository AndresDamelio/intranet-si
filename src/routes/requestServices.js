export default [
    {
      path: "/request-services",
      name: "page-request-services",
      component: () => import("@/views/pages/transactions/request-service/ListView.vue"),
      meta: {
        breadcrumb: [{ title: "Solicitudes de servicio", active: true }],
        rule: "F-SO-AT-02",
        authRequired: true
      }
    },
    {
      path: "/request-service/:id",
      name: "page-edit-request-service",
      component: () => import("@/views/pages/transactions/request-service/Detail.vue"),
      meta: {
        breadcrumb: [
          { title: "Solicitudes de servicio", url: "/request-services" },
          { title: "Detalle", active: true, dynamic: true }
        ],
        rule: "F-SO-AT-02",
        authRequired: true
      }
    }
  ];