export default [
  {
    path: "/apps/calendar/vue-simple-calendar",
    name: "calendar-simple-calendar",
    component: () => import("@/views/apps/calendar/SimpleCalendar.vue"),
    meta: {
      rule: "editor",
      no_scroll: true
    }
  },
  {
    path: "/apps/user/user-list",
    name: "app-user-list",
    component: () => import("@/views/apps/user/user-list/UserList.vue"),
    meta: {
      breadcrumb: [
        { title: "Home", url: "/" },
        { title: "User" },
        { title: "List", active: true }
      ],
      pageTitle: "User List",
      rule: "editor"
    }
  }
];
