export default [
  {
    path: "/centers",
    name: "page-list-view-centers",
    component: () =>
      import("@/views/pages/configuration/web-movil/center/ListView.vue"),
    meta: {
      breadcrumb: [{ title: "Centros", active: true }],
      rule: "A-WM-02",
      authRequired: true
    }
  },
  {
    path: "/center/create",
    name: "page-create-center",
    component: () =>
      import("@/views/pages/configuration/web-movil/center/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Centros", url: "/centers" },
        { title: "Crear", active: true }
      ],
      rule: "A-WM-02",
      authRequired: true
    }
  },
  {
    path: "/center/:id",
    name: "page-edit-center",
    component: () =>
      import("@/views/pages/configuration/web-movil/center/ViewEdit.vue"),
    meta: {
      breadcrumb: [
        { title: "Centros", url: "/centers" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "A-WM-02",
      authRequired: true
    }
  }
];
