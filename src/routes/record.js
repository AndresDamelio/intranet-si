export default [
  {
    path: "/records",
    name: "page-records",
    component: () => import("@/views/pages/transactions/record/ListView.vue"),
    meta: {
      breadcrumb: [{ title: "Pacientes", active: true }],
      rule: "F-EX-01",
      authRequired: true
    }
  },
  {
    path: "/record/:id",
    name: "page-view-record",
    component: () => import("@/views/pages/transactions/record/Detail.vue"),
    meta: {
      breadcrumb: [
        { title: "Pacientes", url: "/records" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "F-EX-01",
      authRequired: true
    }
  }
];
