export default [
    {
      path: "/request-activities",
      name: "page-request-activities",
      component: () => import("@/views/pages/transactions/request-activity/ListView.vue"),
      meta: {
        breadcrumb: [{ title: "Solicitudes de actividad", active: true }],
        rule: "F-SO-AT-03",
        authRequired: true
      }
    },
    {
      path: "/request-activity/:id",
      name: "page-edit-request-activity",
      component: () => import("@/views/pages/transactions/request-activity/Detail.vue"),
      meta: {
        breadcrumb: [
          { title: "Solicitudes de actividad", url: "/request-activities" },
          { title: "Detalle", active: true, dynamic: true }
        ],
        rule: "F-SO-AT-03",
        authRequired: true
      }
    }
  ];