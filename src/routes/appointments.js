export default [
  {
    path: "/appointments",
    name: "page-appointments",
    component: () => import("@/views/pages/transactions/appointment/ListView.vue"),
    meta: {
      breadcrumb: [{ title: "Citas iniciales", active: true }],
      rule: "F-AG-02",
      authRequired: true
    }
  },
  {
    path: "/appointment/:id",
    name: "page-edit-appointment",
    component: () => import("@/views/pages/transactions/appointment/Detail.vue"),
    meta: {
      breadcrumb: [
        { title: "Citas iniciales", url: "/appointments" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "F-AG-02",
      authRequired: true
    }
  }
];
