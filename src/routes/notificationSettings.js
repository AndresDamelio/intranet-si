export default [
    {
      path: "/notification-configs",
      name: "page-notification-configs",
      component: () =>
        import(
          "@/views/pages/configuration/settings/notification-setting/ListView.vue"
        ),
      meta: {
        breadcrumb: [{ title: "Configuraciones de notificación", active: true }],
        rule: "A-CF-03",
        authRequired: true
      }
    },
    {
      path: "/notificati-config/create",
      name: "page-create-notification-config",
      component: () =>
        import(
          "@/views/pages/configuration/settings/notification-setting/Create.vue"
        ),
      meta: {
        breadcrumb: [
          { title: "Configuraciones de notificación", url: "/notification-configs" },
          { title: "Crear", active: true }
        ],
        rule: "A-CF-03",
        authRequired: true
      }
    },
    {
      path: "/notificati-config/:id",
      name: "page-edit-notification-config",
      component: () =>
        import(
          "@/views/pages/configuration/settings/notification-setting/ViewEdit.vue"
        ),
      meta: {
        breadcrumb: [
          { title: "Configuraciones de notificación", url: "/notification-configs" },
          { title: "Detalle", active: true, dynamic: true }
        ],
        rule: "A-CF-03",
        authRequired: true
      }
    }
  ];