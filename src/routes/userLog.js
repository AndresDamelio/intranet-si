export default [
    {
      path: "/user-logs",
      name: "page-userLogs",
      component: () => import("@/views/pages/user-log/ListView.vue"),
      meta: {
        breadcrumb: [{ title: "Accesos de usuario", active: true }],
        rule: "F-BI-01",
        authRequired: true
      }
    },
    {
      path: "/user-log/:id",
      name: "page-edit-u",
      component: () => import("@/views/pages/user-log/Detail.vue"),
      meta: {
        breadcrumb: [
          { title: "Accesos de usuario", url: "/user-logs" },
          { title: "Detalle", active: true, dynamic: true }
        ],
        rule: "F-BI-01",
        authRequired: true
      }
    }
  ];