export default [
  {
    path: "/login",
    name: "page-login",
    component: () => import("@/views/pages/auth/Login.vue"),
    meta: {
      authRequired: false
    }
  },
  {
    path: "/request",
    name: "page-request",
    component: () => import("@/views/pages/auth/Register.vue"),
    meta: {
      authRequired: false
    }
  },
  {
    path: "/forgot-password",
    name: "page-forgot-password",
    component: () => import("@/views/pages/auth/ForgotPassword.vue"),
    meta: {
      authRequired: false
    }
  },
];
