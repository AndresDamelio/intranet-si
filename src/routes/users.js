export default [
  {
    path: "/users",
    name: "page-user",
    component: () =>
      import("@/views/pages/configuration/user/user-list/ListView.vue"),
    meta: {
      breadcrumb: [{ title: "Usuarios", active: true }],
      rule: "F-US-01",
      authRequired: true
    }
  },
  {
    path: "/user/create",
    name: "page-create-user",
    component: () =>
      import("@/views/pages/configuration/user/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Usuarios", url: "/users" },
        { title: "Crear", active: true }
      ],
      rule: "F-US-01",
      authRequired: true
    }
  },
  {
    path: "/user/:id",
    name: "page-view-user",
    component: () => import("@/views/pages/configuration/user/ViewEdit.vue"),
    meta: {
      breadcrumb: [
        { title: "Usuarios", url: "/users" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "F-US-01",
      authRequired: true
    }
  },
  {
    path: "/profile",
    name: "page-profile",
    component: () => import("@/views/pages/configuration/user/Profile.vue"),
    meta: {
      breadcrumb: [
        { title: "Mi perfil", url: null },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "F-US-01",
      authRequired: true
    }
  },
  {
    path: "/change-password",
    name: "page-change-password",
    component: () => import("@/views/pages/auth/ChangePassword.vue"),
    meta: {
      breadcrumb: [
        { title: "Mi perfil", url: '/profile'},
        { title: "Cambiar contraseña", active: true, dynamic: true }
      ],
      rule: "F-US-01",
      authRequired: true
    }
  },
];
