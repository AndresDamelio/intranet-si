export default [
  {
    path: "/requests",
    name: "page-requests",
    component: () => import("@/views/pages/transactions/request/ListView.vue"),
    meta: {
      breadcrumb: [{ title: "Solicitudes de ingreso", active: true }],
      rule: "F-SO-AT-01",
      authRequired: true
    }
  },
  {
    path: "/request/:id",
    name: "page-edit-request",
    component: () => import("@/views/pages/transactions/request/Detail.vue"),
    meta: {
      breadcrumb: [
        { title: "Solicitudes de ingreso", url: "/requests" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "F-SO-AT-01",
      authRequired: true
    }
  }
];
