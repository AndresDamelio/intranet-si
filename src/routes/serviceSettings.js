export default [
  {
    path: "/service-configs",
    name: "page-services-config",
    component: () =>
      import(
        "@/views/pages/configuration/settings/service-setting/ListView.vue"
      ),
    meta: {
      breadcrumb: [{ title: "Configuraciones de servicio", active: true }],
      rule: "A-CF-02",
      authRequired: true
    }
  },
  {
    path: "/service-config/create",
    name: "page-create-service-config",
    component: () =>
      import("@/views/pages/configuration/settings/service-setting/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Configuraciones de servicio", url: "/service-configs" },
        { title: "Crear", active: true }
      ],
      rule: "A-CF-02",
      authRequired: true
    }
  },
  {
    path: "/service-config/:id",
    name: "page-config-service",
    component: () =>
      import(
        "@/views/pages/configuration/settings/service-setting/View.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Configuraciones de servicio", url: "/service-configs" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "A-CF-02",
      authRequired: true
    }
  },
  {
    path: "/service-config/edit/:id",
    name: "page-edit-service-config",
    component: () =>
      import(
        "@/views/pages/configuration/settings/service-setting/Edit.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Configuraciones de servicio", url: "/service-configs" },
        { title: "Editar", active: true, dynamic: true }
      ],
      rule: "A-CF-02",
      authRequired: true
    }
  }
];
