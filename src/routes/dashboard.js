export default [
  {
    path: "/",
    redirect: "/dashboard/resume"
  },
  {
    path: "/dashboard/analytics",
    name: "dashboard-analytics",
    component: () => import("@/views/DashboardAnalytics.vue"),
    meta: {
      rule: "D-02",
      authRequired: true
    }
  },
  {
    path: "/dashboard/resume",
    name: "dashboard-resume",
    component: () => import("@/views/DashboardResume.vue"),
    meta: {
      rule: "D-01",
      authRequired: true
    }
  },
  {
    path: "/dashboard/ecommerce",
    name: "dashboard-ecommerce",
    component: () => import("@/views/DashboardECommerce.vue"),
    meta: {
      authRequired: true
    }
  }
];
