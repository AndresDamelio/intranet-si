export default [
  {
    path: "/telephone-types",
    name: "page-telephone-type",
    component: () =>
      import(
        "@/views/pages/configuration/basic-data/telephone-type/ListView.vue"
      ),
    meta: {
      breadcrumb: [{ title: "Tipos de teléfono", active: true }],
      rule: "A-DB-18",
      authRequired: true
    }
  },
  {
    path: "/telephone-type/create",
    name: "page-create-telephone-type",
    component: () =>
      import(
        "@/views/pages/configuration/basic-data/telephone-type/Create.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Tipos de teléfono", url: "/telephone-types" },
        { title: "Crear", active: true }
      ],
      rule: "A-DB-18",
      authRequired: true
    }
  },
  {
    path: "/telephone-type/:id",
    name: "page-edit-telephone-type",
    component: () =>
      import(
        "@/views/pages/configuration/basic-data/telephone-type/ViewEdit.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Tipos de teléfono", url: "/telephone-types" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "A-DB-18",
      authRequired: true
    }
  }
];
