export default [
  {
    path: "/activity-types",
    name: "page-activity-type",
    component: () =>
      import(
        "@/views/pages/configuration/basic-data/activity-type/ListView.vue"
      ),
    meta: {
      breadcrumb: [{ title: "Tipos de actividad", active: true }],
      rule: "A-DB-12",
      authRequired: true
    }
  },
  {
    path: "/activity-type/create",
    name: "page-create-activity-type",
    component: () =>
      import("@/views/pages/configuration/basic-data/activity-type/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Tipos de actividad", url: "/activity-types" },
        { title: "Crear", active: true }
      ],
      rule: "A-DB-12",
      authRequired: true
    }
  },
  {
    path: "/activity-type/:id",
    name: "page-edit-activity-type",
    component: () =>
      import(
        "@/views/pages/configuration/basic-data/activity-type/ViewEdit.vue"
      ),
    meta: {
      breadcrumb: [
        { title: "Tipos de actividad", url: "/activity-types" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "A-DB-12",
      authRequired: true
    }
  }
];
