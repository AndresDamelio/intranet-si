export default [
  {
    path: "/mobile-web-configuration",
    name: "page-list-view-elements",
    component: () =>
      import(
        "@/views/pages/configuration/web-movil/element/Settings.vue"
      ),
    meta: {
      breadcrumb: [{ title: "Configuración web / móvil", active: true }],
      rule: "A-WM-03",
      authRequired: true
    }
  },
  {
    path: "/element/create",
    name: "page-create-element",
    component: () =>
      import("@/views/pages/configuration/web-movil/element/CreateElement.vue"),
    meta: {
      breadcrumb: [
        { title: "Configuración web / móvil", url: "/mobile-web-configuration" },
        { title: "Crear", active: true }
      ],
      rule: "A-WM-03",
      authRequired: true
    }
  },
  {
    path: "/element/:id",
    name: "page-edit-element",
    component: () =>
      import("@/views/pages/configuration/web-movil/element/ViewEditElement.vue"),
    meta: {
      breadcrumb: [
        { title: "Configuración web / móvil", url: "/mobile-web-configuration" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      rule: "A-WM-03",
      authRequired: true
    }
  }
];
