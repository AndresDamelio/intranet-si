export default [
  {
    path: "/roles",
    name: "page-role",
    component: () =>
      import("@/views/pages/configuration/basic-data/role/ListView.vue"),
    meta: {
      breadcrumb: [{ title: "Roles", active: true }],
      pageTitle: "Roles",
      rule: "A-SF-01",
      authRequired: true
    }
  },
  {
    path: "/role/create",
    name: "page-create-role",
    component: () =>
      import("@/views/pages/configuration/basic-data/role/Create.vue"),
    meta: {
      breadcrumb: [
        { title: "Roles", url: "/roles" },
        { title: "Crear rol", active: true }
      ],
      pageTitle: "Crear rol",
      rule: "A-SF-01",
      authRequired: true
    }
  },
  {
    path: "/role/:id",
    name: "page-edit-role",
    component: () =>
      import("@/views/pages/configuration/basic-data/role/ViewEdit.vue"),
    meta: {
      breadcrumb: [
        { title: "Rol", url: "/roles" },
        { title: "Detalle", active: true, dynamic: true }
      ],
      pageTitle: "Rol",
      rule: "A-SF-01",
      authRequired: true
    }
  },
  {
    path: "/functions",
    name: "page-functions",
    component: () =>
      import("@/views/pages/configuration/basic-data/role/Functions.vue"),
    meta: {
      breadcrumb: [{ title: "Funciones del sistema", active: true }],
      pageTitle: "Funciones del sistema",
      rule: "A-SF-02"
    }
  }
];
