// Translation dictionary for form validations

export const es = {
  es: {
    attributes: {
      name: "nombre",
      description: "descripción",
      title: "título",
      code: "código",
      number: "número",
      telephoneTypeId: "tipo de teléfono",
      question: "pregunta",
      answer: "respuesta",
      address: "dirección",
      slogan: "eslogan",
      mission: "misión",
      vission: "visión",
      aboutUs: "sobre nosotros",
      values: "valores",
      socialFacebook: "facebook",
      socialTwitter: "twitter",
      socialInstagram: "instagram",
      socialLinkedIn: "linkedin",
      socialYouTube: "youtube",
      socialSlack: "slack",
      um: "unidad de medida",
      classResource: "clase de recurso",
      resourceTypeId: "tipo de recurso",
      activityTypeId: "tipo de actividad",
      modality: "modalidad",
      startTime: "hora inicial",
      endTime: "hora final",
      lastname: "apellido",
      email: "correo electrónico",
      birthdate: "fecha de nacimiento",
      parentName: "nombre de familiar",
      datePosible: "fecha tentativa",
      gender: "género",
      serviceTypeId: "tipo de servicio",
      schedules: "turno",
      answerType: "tipo de pregunta",
      userTypeForId: "tipo de usuario",
      userTypes: "tipos de usuario",
      telephone: "teléfono",
      positionId: "cargo",
      specialities: "especialidades",
      urlWeb: "url de la web",
      urlAppAndroid: "url de la aplicación móvil",
      apiKeyGoogleMaps: "api de google maps",
      apiKeyMapBox: "api de mapbox",
      privacyPolicy: "política de privacidad",
      categoryId: "categoría",
      buttonText: "texto del botón",
      buttonColor: "color del botón",
      buttonBackground: "background del boton",
      buttonUrl: "url de redirección del boton",
      buttonPosition: "posición del botón",
      responsible: "responsable",
      block: "bloque de tiempo",
      result: "resultado",
      reasonRejectionId: "motivo de rechazo",
      date: "fecha",
      maritalStatus: "estado civil",
      instructionDegree: "grado de instrucción",
      income: "ingresos",
      expenses: "gastos",
      currency: "moneda",
      occupation: "ocupación",
      pathologyId: "patología",
      origin: "origen",
      kindship: "parentesco",
      notificationTypeId: "tipo de notificación",
      channel: "canal",
      messageNotification: "mensaje",
      serviceConfigId: "configuración de servicio",
      activityConfigId: "configuración de actividad",
      timeBlockId: "bloque",
      scheduleId: "turno",
      capacity: "capacidad",
      resource: "recurso",
      specialists: "especialistas",
      patient: "paciente",
      oldPassword: "contraseña actual",
      newPassword: "nueva contraseña",
      confirmNewPassword: "confirmar contraseña",
      codePatient: "código del paciente",
      time: "hora",
      userId: "usuario",
      placeId: "lugar",
      paramId: "parámetro",
      value: "valor",
      initialValue: "valor inicial",
      finalValue: "valor final",
      user: "usuario",
    },
    messages: {
      excluded: field => `Seleccione un ${field} válido`,
      confirmed: () => "Las contraseñas no coinciden"
    }
  }
};

export const alpha_number_spaces_rule = {
  getMessage: field =>
    `El campo ${field} solo debe contener letras, números, espacios y caracteres ( ,.?¿()).`,
  validate: value => /^[a-z0-9áéíóúñ,.?¿()' ]+$/i.test(value)
};

export const not_equals_rule = {
  getMessage: (field, [target]) =>
    `El campo ${field} no puede ser igual al campo ${target}.`,
  validate: (value, [other]) => value != other
};

export const smaller_than_rule = {
  getMessage: (field, [target]) =>
    `El valor de ${field} debe ser menor que ${target}.`,
  validate: (value, [other]) => {
    value < other;
  }
};
