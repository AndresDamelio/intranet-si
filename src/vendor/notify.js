// function to return personalized notification

export function dialogWhitParameters(
  dialog,
  type,
  color,
  title,
  acceptText,
  cancelText,
  text,
  accept,
  parameters
) {
  return dialog({
    type: type,
    color: color,
    title: title,
    acceptText: acceptText,
    cancelText: cancelText,
    text: text,
    accept: accept,
    parameters: parameters
  });
}

const welcomeNotification = (notify, title, message, color, icon) => {
  return notify({
    title: title,
    time: 5000,
    text: message,
    color: color,
    iconPack: "feather",
    icon: icon
  });
};

const getNotification = (notify, title, message, color, icon) => {
  return notify({
    title: title,
    time: 5000,
    text: message,
    color: color,
    iconPack: "feather",
    icon: icon
  });
};

export function notification(
  type,
  data,
  code,
  notify,
  exception = "Nombre existente",
  title = "Registro no encontrado"
) {
  let error = "";
  switch (type) {
    case "success":
      switch (code) {
        case 201:
          getNotification(
            notify,
            "No se puede eliminar el registro",
            data.message.text,
            "warning",
            "icon-alert-triangle"
          );
          break;
        default:
          getNotification(
            notify,
            "Listo!",
            data.message.text,
            "success",
            "icon-check"
          );
      }
      break;
    case "error":
      switch (code) {
        case 422:
          data.errors.forEach(err => (error += `${" " + err.message}`));
          getNotification(
            notify,
            "Campos obligatorios",
            error,
            "danger",
            "icon-alert-circle"
          );
          break;
        case 409:
          getNotification(
            notify,
            exception,
            data.message.text,
            "warning",
            "icon-alert-circle"
          );
          break;
        case 404:
          getNotification(
            notify,
            title,
            data.message.text,
            "warning",
            "icon-alert-triangle"
          );
          break;
        default:
          getNotification(
            notify,
            "Error",
            "Ha ocurrido un error, recargue la página e intente de nuevo",
            "danger",
            "icon-x-square"
          );
      }
      break;
  }
}

export { getNotification, welcomeNotification };
