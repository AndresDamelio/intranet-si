const phases = [
  {
    id: 1,
    name: null,
    publicName: "Todos",
  },
  {
    id: 2,
    name: "PHASE1",
    publicName: "Estadío I",
  },
  {
    id: 3,
    name: "PHASE2",
    publicName: "Estadío II",
  },
  {
    id: 4,
    name: "PHASE3",
    publicName: "Estadío III",
  },
  {
    id: 4,
    name: "PHASE4",
    publicName: "Estadío IV",
  },
  {
    id: 1,
    name: "PHASE5",
    publicName: "Estadío V",
  },
];

export {
  phases
};