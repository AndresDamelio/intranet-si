const models = [
  {
    name: 'organization',
    publicName: 'organización'
  },
  {
    name: 'position',
    publicName: 'cargo'
  },
  {
    name: 'category',
    publicName: 'categoría'
  },
  {
    name: 'speciality',
    publicName: 'especialidad'
  },
  {
    name: 'place',
    publicName: 'lugar'
  },
  {
    name: 'reasonRejection',
    publicName: 'motivo de rechazo'
  },
  {
    name: 'pathology',
    publicName: 'patología'
  },
  {
    name: 'param',
    publicName: 'parámetro'
  },
  {
    name: 'question',
    publicName: 'pregunta'
  },
  {
    name: 'resource',
    publicName: 'recurso'
  },
  {
    name: 'activityType',
    publicName: 'tipo de actividad'
  },
  {
    name: 'centerType',
    publicName: 'tipo de centro'
  },
  {
    name: 'notificationType',
    publicName: 'tipo de notificación'
  },
  {
    name: 'resourceType',
    publicName: 'tipo de recurso'
  },
  {
    name: 'serviceType',
    publicName: 'tipo de servicio'
  },
  {
    name: 'telephoneType',
    publicName: 'tipo de telefono'
  },
  {
    name: 'schedule',
    publicName: 'turno'
  },
  {
    name: 'role',
    publicName: 'rol'
  },
  {
    name: 'center',
    publicName: 'centro'
  },
  {
    name: 'element',
    publicName: 'elemento'
  },
  {
    name: 'configWebMobile',
    publicName: 'configuracion web móvil'
  },
  {
    name: 'post',
    publicName: 'publicación'
  },
  {
    name: 'faq',
    publicName: 'pregunta frecuente'
  },
  {
    name: 'message',
    publicName: 'mensaje'
  },
  {
    name: 'activityConfig',
    publicName: 'configuración de actividad'
  },
  {
    name: 'serviceConfig',
    publicName: 'configuración de servicio'
  },
  {
    name: 'notification',
    publicName: 'notificación'
  },
  {
    name: 'user',
    publicName: 'usuario'
  },
  {
    name: 'record',
    publicName: 'expediente'
  },
  {
    name: 'appointment',
    publicName: 'cita inicial'
  },
  {
    name: 'service',
    publicName: 'servicio'
  },
  {
    name: 'activity',
    publicName: 'actividad'
  },
  {
    name: 'request',
    publicName: 'solicitud',
    report:true,
    fields:[
      {
        name:'birthdate',
        publicName:'Fecha de nacimiento',
        type: 'date'
      },
      {
        name:'date',
        publicName:'Fecha',
        type: 'date'
      },
      {
        name:'age',
        publicName:'Edad',
        type: 'ageDate'
      },
      {
        name:'gender',
        publicName:'Género',
        values:[
          {
            name:'F',
            publicName:'Femenino'
          },
          {
            name:'M',
            publicName:'Masculino'
          },
        ]
      },
      {
        name:'stage',
        publicName:'Estatus',
        values:[
          {
            name:'PENDING',
            publicName:'Pendiente'
          },
          {
            name:'APPROVED',
            publicName:'Aprobado'
          },
          {
            name:'REJECTED',
            publicName:'Rechazado'
          },
        ]
      },
    ]
  },
]

export{
  models
}