import moment from "moment";
const masters = [
  {
    name: 'organization',
    publicName: 'organización'
  },
  {
    name: 'position',
    publicName: 'cargo'
  },
  {
    name: 'category',
    publicName: 'categoría'
  },
  {
    name: 'speciality',
    publicName: 'especialidad'
  },
  {
    name: 'place',
    publicName: 'lugar'
  },
  {
    name: 'reasonRejection',
    publicName: 'motivo de rechazo'
  },
  {
    name: 'pathology',
    publicName: 'patología'
  },
  {
    name: 'param',
    publicName: 'parámetro'
  },
  {
    name: 'question',
    publicName: 'pregunta'
  },
  {
    name: 'resource',
    publicName: 'recurso'
  },
  {
    name: 'activityType',
    publicName: 'tipo de actividad'
  },
  {
    name: 'centerType',
    publicName: 'tipo de centro'
  },
  {
    name: 'notificationType',
    publicName: 'tipo de notificación'
  },
  {
    name: 'resourceType',
    publicName: 'tipo de recurso'
  },
  {
    name: 'serviceType',
    publicName: 'tipo de servicio'
  },
  {
    name: 'telephoneType',
    publicName: 'tipo de telefono'
  },
  {
    name: 'schedule',
    publicName: 'turno'
  },
  {
    name: 'role',
    publicName: 'rol'
  },
  {
    name: 'center',
    publicName: 'centro'
  },
  {
    name: 'element',
    publicName: 'elemento'
  },
  {
    name: 'configWebMobile',
    publicName: 'configuracion web móvil'
  },
  {
    name: 'post',
    publicName: 'publicación'
  },
  {
    name: 'faq',
    publicName: 'pregunta frecuente'
  },
  {
    name: 'message',
    publicName: 'mensaje'
  },
  {
    name: 'activityConfig',
    publicName: 'configuración de actividad'
  },
  {
    name: 'serviceConfig',
    publicName: 'configuración de servicio'
  },
  {
    name: 'notification',
    publicName: 'notificación'
  },
  {
    name: 'user',
    publicName: 'usuario'
  },
  {
    name: 'record',
    publicName: 'expediente'
  },
  {
    name: 'appointment',
    publicName: 'cita inicial'
  },
  {
    name: 'service',
    publicName: 'servicio'
  },
  {
    name: 'activity',
    publicName: 'actividad'
  },
  {
    name: 'request',
    route: 'requests',
    module: 'request',
    property: 'requests',
    publicName: 'solicitud',
    report:true,
    fields:[
      {
        name:'age',
        publicName:'Edad',
        type: 'ageDate'
      },
      {
        name:'gender',
        publicName:'Género',
        values:[
          {
            name:'F',
            publicName:'Femenino'
          },
          {
            name:'M',
            publicName:'Masculino'
          },
        ]
      },
      {
        name:'stage',
        publicName:'Estatus',
        values:[
          {
            name:'PENDING',
            publicName:'Pendiente'
          },
          {
            name:'APPROVED',
            publicName:'Aprobado'
          },
          {
            name:'REJECTED',
            publicName:'Rechazado'
          },
        ]
      },
    ],
    columns: [
      {
        headerName: "Fecha",
        field: "date",
        filter: "agDateColumnFilter",
        valueGetter: function(params) {
          return  moment(String(params.data.date)).format("DD/MM/YYYY")
        },
        filterParams: {
          comparator: (filterLocalDateAtMidnight, cellValue) => {
            var dateAsString = cellValue;
            if (dateAsString == null) return -1;
            var dateParts = dateAsString.split("/");
            var cellDate = new Date(
              Number(dateParts[2]),
              Number(dateParts[1]) - 1,
              Number(dateParts[0])
            );
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true
        },
        width: 150,
        suppressSizeToFit: true
      },
      {
        headerName: "Nombre",
        filter: true,
        valueGetter: function(params) {
          //return  moment(String(params.data.date)).format("dd/mm/yyyy")
          return params.data.name + " " + params.data.lastname;
        }
      },
      {
        headerName: "Edad (años)",
        field: "birthdate",
        filter: "agNumberColumnFilter",
        valueGetter: function(params) {
          return moment().diff(moment(params.data.birthdate, "YYYY/MM/DD"), 'years');
        },
      },
      {
        headerName: "Estado",
        field: "stage",
        filter: true,
        cellRenderer: "statusRenderer",
        cellClass: "cell-status"
      }
    ],
  },
]

export{
  masters
}