const answerType = [
  {
    id: 1,
    name: "QUALITY",
    publicName: "Por escala de calidad",
    options: ["Malo", "Regular", "Bueno", "Excelente"],
    minValue: 1,
    maxValue: 4,
    class: "Linkert"
  },
  {
    id: 2,
    name: "FREQUENCY",
    publicName: "Por escala temporal",
    options: ["Nunca", "Casi nunca", "A veces", "Casi siempre", "Siempre"],
    minValue: 1,
    maxValue: 5,
    class: "Linkert"
  },
  {
    id: 3,
    name: "IMPORTANCE",
    publicName: "Por escala de importancia",
    options: [
      "No es importante",
      "Poco importante",
      "Neutral",
      "Importante",
      "Muy importante"
    ],
    minValue: 1,
    maxValue: 5,
    class: "linkert"
  },
  {
    id: 4,
    name: "DIFFICULTY",
    publicName: "Por escala de dificultad",
    options: ["Muy difícil", "Difícil", "Neutral", "Fácil", "Muy fácil"],
    minValue: 1,
    maxValue: 5,
    class: "linkert"
  },
  {
    id: 5,
    name: "SATISFACTION",
    publicName: "Por escala de satisfacción",
    options: [
      "Muy satisfecho",
      "Satisfecho",
      "Poco satisfecho",
      "Insatisfecho"
    ],
    minValue: 1,
    maxValue: 4,
    class: "linkert"
  },
  {
    id: 6,
    name: "LEVEL",
    publicName: "Por escala de nivel",
    options: ["Alto", "Normal", "Bajo"],
    minValue: 1,
    maxValue: 3,
    class: "linkert"
  },
  {
    id: 7,
    name: "YESNO",
    publicName: "Dicotómica (Si o No)",
    options: ["Si", "No"],
    minValue: 1,
    maxValue: 2,
    class: "dicotomica"
  },
  {
    id: 8,
    name: "STARS",
    publicName: "5 estrellas",
    options: [
      "1 Estrella",
      "2 Estrellas",
      "3 Estrellas",
      "4 Estrellas",
      "5 Estrellas"
    ],
    minValue: 1,
    maxValue: 5,
    class: "stars"
  }
];

export {
  answerType
};
