const channel = [
    {
      id: 1,
      name: "EMAIL",
      publicName: "Correo",
    },
    {
      id: 2,
      name: "MOVIL",
      publicName: "Móvil",
    },
    {
      id: 3,
      name: "Intranet",
      publicName: "Interna",
    },
  ];
  
  export {
    channel
  };
  