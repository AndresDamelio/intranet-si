const resourceClass = [
  {
    id: 1,
    name: "SERVICE",
    publicName: "Servicios",
  },
  {
    id: 2,
    name: "ACTIVITY",
    publicName: "Actividades",
  },
  {
    id: 3,
    name: "APPOINTMENT",
    publicName: "Cita inicial",
  },
];

export {
  resourceClass
};
