import Vue from "vue";
import Router from "vue-router";
import store from "./store/store.js";

import routesDashboard from "@/routes/dashboard.js";
import routesApplication from "@/routes/application.js";
import routesElementsUI from "@/routes/elementsUI.js";
import routesComponents from "@/routes/components.js";
import routesFormsElements from "@/routes/formsElements.js";
import routesPages from "@/routes/pages.js";
import routesActivityTypes from "@/routes/activityTypes.js";
import routesServiceTypes from "@/routes/serviceTypes.js";
import routesMessageTypes from "@/routes/messageTypes.js";
import routesSpecialties from "@/routes/specialties.js";
import routesPositions from "@/routes/positions.js";
import routesCategories from "@/routes/categories.js";
import routesNotificationTypes from "@/routes/notificationTypes.js";
import routesPathologies from "@/routes/pathologies.js";
import routesCenterTypes from "@/routes/centerTypes.js";
import routesOrganization from "@/routes/organization.js";
import routesChartsAndMaps from "@/routes/chartsAndMaps.js";
import routesExtensions from "@/routes/extensions";
import routesTelephoneTypes from "@/routes/telephoneTypes.js";
import routesFunctionalSafety from "@/routes/functionalSafety.js";
import routesReasonRejections from "@/routes/reasonRejections.js";
import routesResourceTypes from "@/routes/resourceTypes.js";
import routesPlaces from "@/routes/places.js";
import routesFaqs from "@/routes/faqs.js";
import routesParams from "@/routes/params.js";
import routesResources from "@/routes/resources.js";
import routesSchedules from "@/routes/schedules.js";
import routeActivitySettings from "@/routes/activitySettings.js";
import routesRequests from "@/routes/requests.js";
import routeServiceSettings from "@/routes/serviceSettings.js";
import routesQuestions from "@/routes/questions.js";
import routesCenters from "@/routes/centers.js";
import routesElements from "@/routes/elements.js";
import routesUsers from "@/routes/users.js";
import routesDatabase from "@/routes/database.js";
import routesAuth from "@/routes/auth.js";
import routesAppointments from "@/routes/appointments.js";
import routeMiscellaneous from "@/routes/miscellaneous.js";
import routesPosts from "@/routes/posts.js";
import routesReports from "@/routes/reports.js";
import routesRecord from "@/routes/record.js";
import routesMessages from "@/routes/messages.js";
import routesNotificationSettings from "@/routes/notificationSettings.js";
import routesUserLogs from "@/routes/userLog.js";
import routesRequestServices from "@/routes/requestServices.js";
import routesRequestActivities from "@/routes/requestActivities.js";
import routesServices from "@/routes/services.js";
import routeActivities from "@/routes/activities.js";
import routesCalendar from "@/routes/calendar.js";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  routes: [
    {
      /* ===================== MAIN LAYOUT ROUTES =====================*/
      path: "",
      component: () => import("./layouts/main/Main.vue"),
      children: [
        /* ===================== Dashboard =====================*/
        ...routesDashboard,
        /* ===================== Application =====================*/
        ...routesApplication,
        /* ===================== UI Elements =====================*/
        ...routesElementsUI,
        /* ===================== Components =====================*/
        ...routesComponents,
        /* ===================== Forms Elements =====================*/
        ...routesFormsElements,
        /* ===================== Pages =====================*/
        ...routesPages,
        /* ===================== Activity Types =====================*/
        ...routesActivityTypes,
        /* ===================== Service Types =====================*/
        ...routesServiceTypes,
        /* ===================== Message Types =====================*/
        ...routesMessageTypes,
        /* ===================== Specialties =====================*/
        ...routesSpecialties,
        /* ===================== Positions =====================*/
        ...routesPositions,
        /* ===================== Categories =====================*/
        ...routesCategories,
        /* ===================== Notification Types =====================*/
        ...routesNotificationTypes,
        /* ===================== Pathologies =====================*/
        ...routesPathologies,
        /* ===================== Center Types =====================*/
        ...routesCenterTypes,
        /* ===================== Organization =====================*/
        ...routesOrganization,
        /* ===================== Charts and maps =====================*/
        ...routesChartsAndMaps,
        /* ===================== Extensions =====================*/
        ...routesExtensions,
        /* ===================== Telephone Types =====================*/
        ...routesTelephoneTypes,
        /* ===================== Functional Safety =====================*/
        ...routesFunctionalSafety,
        /* ===================== Reason Rejections =====================*/
        ...routesReasonRejections,
        /* ===================== Resource Types =====================*/
        ...routesResourceTypes,
        /* ===================== Places =====================*/
        ...routesPlaces,
        /* ===================== Faqs =====================*/
        ...routesFaqs,
        /* ===================== Params =====================*/
        ...routesParams,
        /* ===================== Resources =====================*/
        ...routesResources,
        /* ===================== Schedules =====================*/
        ...routesSchedules,
        /* ===================== Activity Settings =====================*/
        ...routeActivitySettings,
        /* ===================== Requests =====================*/
        ...routesRequests,
        /* ===================== Service Settings =====================*/
        ...routeServiceSettings,
        /* ===================== Questions =====================*/
        ...routesQuestions,
        /* ===================== Centers =====================*/
        ...routesCenters,
        /* ===================== Elements =====================*/
        ...routesElements,
        /* ===================== Users =====================*/
        ...routesUsers,
        /* ===================== Database =====================*/
        ...routesDatabase,
        /* ===================== Posts =====================*/
        ...routesPosts,
        /* ===================== Services =====================*/
        ...routesServices,
        /* ===================== Appointment =====================*/
        ...routesAppointments,
        /* ===================== Reports ====================== */
        ...routesReports,
        /* ===================== Record ====================== */
        ...routesRecord,
        /* ===================== Messages =====================*/
        ...routesMessages,
        /* ===================== Activities =====================*/
        ...routeActivities,
        /* ===================== Notification Settings=====================*/
        ...routesNotificationSettings,
        /* ===================== User Logs=====================*/
        ...routesUserLogs,
        /* ===================== Request Services=====================*/
        ...routesRequestServices,
        /* ===================== Request Activities=====================*/
        ...routesRequestActivities,
        /* ===================== Calendar =====================*/
        ...routesCalendar
      ]
    },
    // =============================================================================
    // FULL PAGE LAYOUTS
    // =============================================================================
    {
      path: "",
      component: () => import("@/layouts/full-page/FullPage.vue"),
      children: [
        /* ===================== Auth =====================*/
        ...routesAuth,
        /* ===================== Miscellaneous =====================*/
        ...routeMiscellaneous
      ]
    },
    // Redirect to 404 page, if no match found
    {
      path: "*",
      redirect: "/error-404"
    }
  ]
});

router.afterEach(() => {
  // Remove initial loading
  const appLoading = document.getElementById("loading-bg");
  if (appLoading) {
    appLoading.style.display = "none";
  }
});

router.beforeEach((to, from, next) => {
  const ability = store.getters["auth/ability"];
  ability.update([store.state.auth.rules]);

  if (to.meta.authRequired) {
    if (!store.state.auth.isAuthenticated) {
      router.push({ path: "/login" });
    } else {
      if(!ability.can(to.meta.rule, "userLogged")) {
        router.push({ path: "/not-authorized" });
      }
    }
  } else {
    if (store.state.auth.isAuthenticated) {
      if (
        to.path === "/login" ||
        to.path === "/forgot-password" ||
        to.path === "/error-404" ||
        to.path === "/error-500" ||
        to.path === "/request" ||
        to.path === "/callback" ||
        to.path === "/comingsoon"
      ) {
        router.push({ path: "/" });
      }
    }
  }

  return next();
});

export default router;
