import moment from "moment";

export function getEventArray(appointments, activities, services) {
  appointments.map(e => {
    delete e["id"];
    e["title"] = "Cita";
    e["startDate"] = e["date"];
    e["classes"] = "event-danger";
    e["startTime"] = e["timeBlock"]["timeStart"];
    e["endTime"] = e["timeBlock"]["timeEnd"];
    e["label"] = "Cita";
  });

  activities.forEach(e => {
    delete e["id"];
    e["title"] = e["name"];
    delete e["name"];
    e["startDate"] = e["date"];
    if (e["schedule"]) {
      e["startTime"] = e["schedule"]["timeStart"];
      e["endTime"] = e["schedule"]["timeEnd"];
      delete e["schedule"];
    } else {
      e["startTime"] = e["timeBlock"]["timeStart"];
      e["endTime"] = e["timeBlock"]["timeEnd"];
      delete e["timeBlock"];
    }
    e["classes"] = "event-warning";
    e["label"] = "Actividad";
  });

  moment.locale("es");
  services.forEach(e => {
    delete e["id"];
    e["title"] = e["name"];
    delete e["name"];
    e["startDate"] = e["date"];
    if (e["schedule"]) {
      e["startTime"] = e["schedule"]["timeStart"];
      e["endTime"] = e["schedule"]["timeEnd"];
      delete e["schedule"];
    } else {
      e["startTime"] = e["timeBlock"]["timeStart"];
      e["endTime"] = e["timeBlock"]["timeEnd"];
      delete e["timeBlock"];
    }
    e["classes"] = "event-success";
    e["label"] = "Servicio";
  });

  return appointments.concat(activities, services);
}
