export default {
  ADD_ITEM(state, item) {
    state.questions.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.questions.findIndex(index => index.id == item.id);
    Object.assign(state.questions[index], item);
  },

  SET_ITEMS(state, items) {
    state.questions = items;
  },

  SET_ITEM(state, item) {
    state.question = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.questions.findIndex(index => index.id == id);
    state.questions.splice(index, 1);
  },
};
