export default {
  getAnswerTypes: state => {
    return state.answerTypes.map(answerType => {
      return {
        label: answerType.publicName,
        val: answerType.name,
        options: answerType.options,
        class: answerType.class
      };
    });
  },
  getCustomItems: state => {
    return state.answerTypes
      .filter(answerType => answerType.status == 1)
      .map(answerType => {
        return {
          label: answerType.title,
          val: answerType.id
        };
      });
  },

  getQuestions: state => {
    return state.questions
      .filter(question => question.status == 1)
      .map(question => {
        return {
          label: question.title,
          id: question.id
        };
      });
  }
};
