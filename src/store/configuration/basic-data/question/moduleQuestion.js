import state from "./moduleQuestionState.js";
import mutations from "./moduleQuestionMutations.js";
import actions from "./moduleQuestionActions.js";
import getters from "./moduleQuestionGetters.js";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
