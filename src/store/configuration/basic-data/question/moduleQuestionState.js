import { answerType } from "@/fake-db/data/basic/answer-type.js"

export default {
  questions: [],
  question: null,
  answerTypes: answerType,
};
