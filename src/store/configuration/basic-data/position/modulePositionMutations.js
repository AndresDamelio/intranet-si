export default {
  ADD_ITEM(state, item) {
    state.positions.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.positions.findIndex(index => index.id == item.id);
    Object.assign(state.positions[index], item);
  },

  SET_ITEMS(state, items) {
    state.positions = items;
  },

  SET_ITEM(state, item) {
    state.position = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.positions.findIndex(index => index.id == id);
    state.positions.splice(index, 1);
  }
};
