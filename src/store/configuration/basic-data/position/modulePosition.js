import state from "./modulePositionState";
import actions from "./modulePositionActions";
import getters from "./modulePositionGetters";
import mutations from "./modulePositionMutations";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
