import state from "./moduleUserTypeState.js";
import mutations from "./moduleUserTypeMutations.js";
import actions from "./moduleUserTypeActions.js";
import getters from "./moduleUserTypeGetters.js";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
