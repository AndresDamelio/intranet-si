export default {
  getCustomItems: state => {
    return state.userTypes
      .map(userType => {
        return {
          label: userType.name,
          id: userType.id
        };
      });
  },
  getPhases: state => {
    return state.phases
      .map(phase => {
        return {
          label: phase.publicName,
          val: phase.name
        };
      });
  },
  getPatientId: state => {
    let userType = state.userTypes.find( item => item.name.toLocaleLowerCase() == 'paciente');
    return userType.id
  }
};
