import Vue from "vue";

export default {
  getFunctions({ commit }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .get("functions")
        .then(resp => {
          commit(`role/SET_FUNCTIONS`, resp.body.functions, { root: true });
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  }
};
