export default {
  ADD_ITEM(state, item) {
    state.roles.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.roles.findIndex(index => index.id == item.id);
    Object.assign(state.roles[index], item);
  },

  SET_ITEMS(state, items) {
    state.roles = items;
  },

  SET_ITEM(state, item) {
    state.role = item;
    if (item) state.role.functions = item.functions.map(f => f.id);
  },

  SET_FUNCTIONS(state, items) {
    state.functions = items;
    for (let i = 0; i < state.functions.length; i++) {
      state.functions[i].selected = false;
    }
  },

  DELETE_ITEM(state, id) {
    let index = state.roles.findIndex(index => index.id == id);
    state.roles.splice(index, 1);
  }
};
