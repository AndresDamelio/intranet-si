export default {
  ADD_ITEM(state, item) {
    state.activityTypes.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.activityTypes.findIndex(index => index.id == item.id);
    Object.assign(state.activityTypes[index], item);
  },

  SET_ITEMS(state, items) {
    state.activityTypes = items;
  },

  SET_ITEM(state, item) {
    state.activityType = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.activityTypes.findIndex(index => index.id == id);
    state.activityTypes.splice(index, 1);
  }
};
