export default {
  getCustomItems: state => {
    return state.activityTypes
      .filter(activityType => activityType.status === 1)
      .map(activityType => {
        return {
          label: activityType.name,
          val: activityType.id
        };
      });
  }
};