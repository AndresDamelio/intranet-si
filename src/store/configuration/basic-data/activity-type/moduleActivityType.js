import state from "./moduleActivityTypeState.js";
import mutations from "./moduleActivityTypeMutations.js";
import actions from "./moduleActivityTypeActions.js";
import getters from "./moduleActivityTypeGetters.js";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
