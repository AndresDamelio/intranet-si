import state from "./moduleServiceTypeState.js";
import mutations from "./moduleServiceTypeMutations.js";
import actions from "./moduleServiceTypeActions.js";
import getters from "./moduleServiceTypeGetters.js";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
