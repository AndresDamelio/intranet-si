export default {
  getCustomItems: state => {
    return state.serviceTypes
      .filter(serviceType => serviceType.status === 1)
      .map(serviceType => {
        return {
          label: serviceType.name,
          val: serviceType.id
        };
      });
  }
};
