export default {
  ADD_ITEM(state, item) {
    state.serviceTypes.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.serviceTypes.findIndex(index => index.id == item.id);
    Object.assign(state.serviceTypes[index], item);
  },

  SET_ITEMS(state, items) {
    state.serviceTypes = items;
  },

  SET_ITEM(state, item) {
    state.serviceType = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.serviceTypes.findIndex(index => index.id == id);
    state.serviceTypes.splice(index, 1);
  }
};
