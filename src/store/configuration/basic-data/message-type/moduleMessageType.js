import state from "./moduleMessageTypeState.js";
import mutations from "./moduleMessageTypeMutations.js";
import actions from "./moduleMessageTypeActions.js";
import getters from "./moduleMessageTypeGetters.js";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
