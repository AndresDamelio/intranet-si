export default {
  ADD_ITEM(state, item) {
    state.messageTypes.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.messageTypes.findIndex(index => index.id == item.id);
    Object.assign(state.messageTypes[index], item);
  },

  SET_ITEMS(state, items) {
    state.messageTypes = items;
  },

  SET_ITEM(state, item) {
    state.messageType = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.messageTypes.findIndex(index => index.id == id);
    state.messageTypes.splice(index, 1);
  }
};
