export default {
  ADD_ITEM(state, item) {
    state.places.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.places.findIndex(index => index.id == item.id);
    Object.assign(state.places[index], item);
  },

  SET_ITEMS(state, items) {
    state.places = items;
  },

  SET_ITEM(state, item) {
    state.place = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.places.findIndex(index => index.id == id);
    state.places.splice(index, 1);
  }
};
