export default {
  getCustomItems: state => {
    return state.places
      .filter(place => place.status === 1)
      .map(place => {
        return {
          label: place.name,
          val: place.id
        };
      });
  },
};
