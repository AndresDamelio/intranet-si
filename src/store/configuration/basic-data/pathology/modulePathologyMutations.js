export default {
  ADD_ITEM(state, item) {
    state.pathologies.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.pathologies.findIndex(index => index.id == item.id);
    Object.assign(state.pathologies[index], item);
  },

  SET_ITEMS(state, items) {
    state.pathologies = items;
  },

  SET_ITEM(state, item) {
    state.pathology = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.pathologies.findIndex(index => index.id == id);
    state.pathologies.splice(index, 1);
  }
};
