import state from "./modulePathologyState.js";
import mutations from "./modulePathologyMutations.js";
import actions from "./modulePathologyActions.js";
import getters from "./modulePathologyGetters.js";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
