export default {
  getCustom: state => {
    let items = state.pathologies.map(el => {
      return {
        label: el.name,
        val: el.id
      }
    })
    return items;
  }
};
