import state from "./moduleBlockState";
import actions from "./moduleBlockActions";
import getters from "./moduleBlockGetters";
import mutations from "./moduleBlockMutations";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
