export default {
  ADD_ITEM(state, item) {
    state.blocks.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.blocks.findIndex(index => index.id == item.id);
    Object.assign(state.blocks[index], item);
  },

  SET_ITEMS(state, items) {
    state.blocks = items;
  },

  SET_ITEM(state, item) {
    state.block = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.blocks.findIndex(index => index.id == id);
    state.blocks.splice(index, 1);
  }
};
