import moment from "moment";

function timeFormat (value) {
  let date = new Date();
  if (value) {
    date.setHours(parseInt(value.split(":")[0]));
    date.setMinutes(parseInt(value.split(":")[1]));
    return moment(String(date)).format("hh:mm A");
  }
}
export default {
  getCustomItems: state => {
    return state.blocks
      .map(block => {
        return {
          label: timeFormat(block.timeStart)+" - "+timeFormat(block.timeEnd),
          val: block.id
        };
      });
  }
};
