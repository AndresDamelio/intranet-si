import { resourceClass } from "@/fake-db/data/basic/resource-class.js"

export default {
  resources: [],
  resource: null,
  resourceClasses: resourceClass
};
