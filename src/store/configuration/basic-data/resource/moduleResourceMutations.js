export default {
  ADD_ITEM(state, item) {
    state.resources.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.resources.findIndex(index => index.id == item.id);
    Object.assign(state.resources[index], item);
  },

  SET_ITEMS(state, items) {
    state.resources = items;
  },

  SET_ITEM(state, item) {
    state.resource = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.resources.findIndex(index => index.id == id);
    state.resources.splice(index, 1);
  }
};
