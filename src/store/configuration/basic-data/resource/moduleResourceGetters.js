export default {
  getCustomItems: state => {
    return state.resources
      .filter(resource => resource.status === 1)
      .map(resource => {
        return {
          label: resource.name,
          id: resource.id
        };
      });
  },

  getResourceClasses: state => {
    return state.resourceClasses.map(resourceClass => {
      return {
        label: resourceClass.publicName,
        val: resourceClass.name
      };
    });
  },
  getCustomResources: state => type => {
    return state.resources
      .filter(
        resource =>
          resource.status === 1 &&
          resource.classResource
            .toLocaleLowerCase()
            .split(",")
            .includes(type)
      )
      .map(resource => {
        return {
          label: resource.name,
          id: resource.id
        };
      });
  }
};
