export default {
  ADD_ITEM(state, item) {
    state.notificationTypes.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.notificationTypes.findIndex(index => index.id == item.id);
    Object.assign(state.notificationTypes[index], item);
  },

  SET_ITEMS(state, items) {
    state.notificationTypes = items;
  },

  SET_ITEM(state, item) {
    state.notificationType = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.notificationTypes.findIndex(index => index.id == id);
    state.notificationTypes.splice(index, 1);
  }
};
