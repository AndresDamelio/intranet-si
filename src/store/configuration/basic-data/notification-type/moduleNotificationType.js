import state from "./moduleNotificationTypeState.js";
import mutations from "./moduleNotificationTypeMutations.js";
import actions from "./moduleNotificationTypeActions.js";
import getters from "./moduleNotificationTypeGetters.js";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
