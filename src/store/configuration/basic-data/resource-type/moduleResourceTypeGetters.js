export default {
  getCustomItems: state => {
    return state.resourceTypes
      .filter(resourceType => resourceType.status === 1)
      .map(resourceType => {
        return {
          label: resourceType.name,
          val: resourceType.id
        };
      });
  }
};
