import state from "./moduleResourceTypeState.js";
import mutations from "./moduleResourceTypeMutations.js";
import actions from "./moduleResourceTypeActions.js";
import getters from "./moduleResourceTypeGetters.js";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
