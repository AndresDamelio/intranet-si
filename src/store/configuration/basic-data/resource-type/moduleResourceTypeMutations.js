export default {
  ADD_ITEM(state, item) {
    state.resourceTypes.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.resourceTypes.findIndex(index => index.id == item.id);
    Object.assign(state.resourceTypes[index], item);
  },

  SET_ITEMS(state, items) {
    state.resourceTypes = items;
  },

  SET_ITEM(state, item) {
    state.resourceType = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.resourceTypes.findIndex(index => index.id == id);
    state.resourceTypes.splice(index, 1);
  }
};
