export default {
  ADD_ITEM(state, item) {
    state.specialities.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.specialities.findIndex(index => index.id == item.id);
    Object.assign(state.specialities[index], item);
  },

  SET_ITEMS(state, items) {
    state.specialities = items;
  },

  SET_ITEM(state, item) {
    state.speciality = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.specialities.findIndex(index => index.id == id);
    state.specialities.splice(index, 1);
  }
};
