import state from "./moduleSpecialityState.js";
import mutations from "./moduleSpecialityMutations.js";
import actions from "./moduleSpecialityActions.js";
import getters from "./moduleSpecialityGetters.js";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
