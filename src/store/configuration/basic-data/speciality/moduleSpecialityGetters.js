export default {
  getCustomItems: state => {
    return state.specialities
      .filter(speciality => speciality.status === 1)
      .map(speciality => {
        return {
          label: speciality.name,
          id: speciality.id
        };
      });
  }
};
