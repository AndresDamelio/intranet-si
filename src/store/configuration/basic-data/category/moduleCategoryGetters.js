export default {
    getCustomItems: state => {
      return state.categories
        .filter(category => category.status === 1)
        .map(category => {
          return {
            label: category.name,
            val: category.id
          };
        });
    }
  };