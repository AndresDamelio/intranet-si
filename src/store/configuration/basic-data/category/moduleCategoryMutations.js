export default {
  ADD_ITEM(state, item) {
    state.categories.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.categories.findIndex(index => index.id == item.id);
    Object.assign(state.categories[index], item);
  },

  SET_ITEMS(state, items) {
    state.categories = items;
  },

  SET_ITEM(state, item) {
    state.category = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.categories.findIndex(index => index.id == id);
    state.categories.splice(index, 1);
  }
};
