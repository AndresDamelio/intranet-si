export default {
    getCustomItems: state => {
      return state.params
        .filter(param => param.status === 1)
        .map(param => {
          return {
            label: param.name,
            id: param.id
          };
        });
    }
  };
  