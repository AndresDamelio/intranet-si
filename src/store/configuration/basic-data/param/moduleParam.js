import state from "./moduleParamState.js";
import mutations from "./moduleParamMutations.js";
import actions from "./moduleParamActions.js";
import getters from "./moduleParamGetters.js";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
