export default {
  ADD_ITEM(state, item) {
    state.params.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.params.findIndex(index => index.id == item.id);
    Object.assign(state.params[index], item);
  },

  SET_ITEMS(state, items) {
    state.params = items;
  },

  SET_ITEM(state, item) {
    state.param = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.params.findIndex(index => index.id == id);
    state.params.splice(index, 1);
  }
};
