export default {
  ADD_ITEM(state, item) {
    state.schedules.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.schedules.findIndex(index => index.id == item.id);
    Object.assign(state.schedules[index], item);
  },

  SET_ITEMS(state, items) {
    state.schedules = items;
  },

  SET_ITEM(state, item) {
    state.schedule = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.schedules.findIndex(index => index.id == id);
    state.schedules.splice(index, 1);
  }
};
