import state from "./moduleScheduleState";
import actions from "./moduleScheduleActions";
import getters from "./moduleScheduleGetters";
import mutations from "./moduleScheduleMutations";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
