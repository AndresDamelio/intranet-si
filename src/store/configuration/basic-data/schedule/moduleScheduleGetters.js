export default {
  getCustomItems: state => {
    return state.schedules
      .filter(schedule => schedule.status === 1)
      .map(schedule => {
        return {
          label: schedule.name,
          val: schedule.id
        };
      });
  }
};
