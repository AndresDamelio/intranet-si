import state from "./moduleReasonRejectionState";
import actions from "./moduleReasonRejectionActions";
import getters from "./moduleReasonRejectionGetters";
import mutations from "./moduleReasonRejectionMutations";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
