export default {
  ADD_ITEM(state, item) {
    state.reasonRejections.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.reasonRejections.findIndex(index => index.id == item.id);
    Object.assign(state.reasonRejections[index], item);
  },

  SET_ITEMS(state, items) {
    state.reasonRejections = items;
  },

  SET_ITEM(state, item) {
    state.reasonRejection = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.reasonRejections.findIndex(index => index.id == id);
    state.reasonRejections.splice(index, 1);
  }
};
