export default {
  ADD_ITEM(state, item) {
    state.centerTypes.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.centerTypes.findIndex(index => index.id == item.id);
    Object.assign(state.centerTypes[index], item);
  },

  SET_ITEMS(state, items) {
    state.centerTypes = items;
  },

  SET_ITEM(state, item) {
    state.centerType = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.centerTypes.findIndex(index => index.id == id);
    state.centerTypes.splice(index, 1);
  }
};
