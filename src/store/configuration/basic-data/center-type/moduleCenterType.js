import state from "./moduleCenterTypeState.js";
import mutations from "./moduleCenterTypeMutations.js";
import actions from "./moduleCenterTypeActions.js";
import getters from "./moduleCenterTypeGetters.js";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
