export default {
  getCustomItems: state => {
    return state.telephoneTypes
      .filter(telephoneType => telephoneType.status === 1)
      .map(telephoneType => {
        return {
          label: telephoneType.name,
          val: telephoneType.id
        };
      });
  }
};
