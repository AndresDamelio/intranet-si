export default {
  ADD_ITEM(state, item) {
    state.telephoneTypes.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.telephoneTypes.findIndex(index => index.id == item.id);
    Object.assign(state.telephoneTypes[index], item);
  },

  SET_ITEMS(state, items) {
    state.telephoneTypes = items;
  },

  SET_ITEM(state, item) {
    state.telephoneType = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.telephoneTypes.findIndex(index => index.id == id);
    state.telephoneTypes.splice(index, 1);
  }
};
