import state from "./moduleTelephoneTypeState.js";
import mutations from "./moduleTelephoneTypeMutations.js";
import actions from "./moduleTelephoneTypeActions.js";
import getters from "./moduleTelephoneTypeGetters.js";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
