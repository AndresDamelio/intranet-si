import Vue from "vue";

export default {
  getItems({ commit }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .get("configWebMobiles")
        .then(resp => {
          commit("SET_ITEM", resp.body.configWebMobiles[0]);
          if (resp.body.configWebMobiles.length) {
            commit("THERE_CONFIG", true);
          } else {
            commit("THERE_CONFIG", false);
          }
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  getItem({ commit }, id) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .get(`configWebMobiles/${id}`)
        .then(resp => {
          commit("SET_ITEM", resp.body.configWebMobile);
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  }
};
