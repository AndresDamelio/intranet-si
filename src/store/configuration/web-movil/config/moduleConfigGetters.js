export default {
  getConfig: state => {
    return state.config;
  },

  thereConfig: state => {
    return state.thereConfig;
  }
};
