export default {
  ADD_ITEM(state, item) {
    state.config = item;
    state.thereConfig = true;
  },

  UPDATE_ITEM(state, item) {
    state.config = item;
  },

  SET_ITEM(state, item) {
    state.config = item;
  },

  THERE_CONFIG(state, value) {
    state.thereConfig = value;
  }
};
