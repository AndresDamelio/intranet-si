import state from "./moduleConfigState";
import mutations from "./moduleConfigMutations";
import actions from "./moduleConfigActions";
import getters from "./moduleConfigGetters";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
