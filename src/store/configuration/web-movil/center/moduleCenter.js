import state from "./moduleCenterState";
import mutations from "./moduleCenterMutations";
import actions from "./moduleCenterActions";
import getters from "./moduleCenterGetters";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
