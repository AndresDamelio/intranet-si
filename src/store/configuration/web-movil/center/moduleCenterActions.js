let urlBase = new URL("https://maps.googleapis.com/maps/api/geocode/json?");

let params = [];

export default {
  // eslint-disable-next-line no-empty-pattern
  getCoordinates({}, { address }) {
    address = address.split(" ").join("+");

    params = { address, key: "AIzaSyBjMy1DJ65QsP6iWi3JMpAj5XaYV7GpKjI" };

    urlBase.search = new URLSearchParams(params).toString();
    //Avenida A con Avenida B urbanizacion el Sisal Barquisimeto

    return new Promise(async (resolve, reject) => {
      fetch(urlBase)
        .then(res => res.json())
        .catch(err => reject(err))
        .then(response => {
          resolve(response);
        });
    });
  },
  
};
