export default {
  ADD_ITEM(state, item) {
    state.centers.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.centers.findIndex(index => index.id == item.id);
    Object.assign(state.centers[index], item);
  },

  SET_ITEMS(state, items) {
    state.centers = items;
  },

  SET_ITEM(state, item) {
    state.center = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.centers.findIndex(index => index.id == id);
    state.centers.splice(index, 1);
  }
};
