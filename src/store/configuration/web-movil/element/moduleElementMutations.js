export default {
  ADD_ITEM(state, item) {
    state.elements.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.elements.findIndex(index => index.id == item.id);
    Object.assign(state.elements[index], item);
  },

  SET_ITEMS(state, items) {
    state.elements = items;
  },

  SET_ITEM(state, item) {
    state.element = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.elements.findIndex(index => index.id == id);
    state.elements.splice(index, 1);
  }
};
