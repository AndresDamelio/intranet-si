import state from "./moduleElementState";
import mutations from "./moduleElementMutations";
import actions from "./moduleElementActions";
import getters from "./moduleElementGetters";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
