export default {
    getCustomItems: state => {
      return state.faqs
        .filter(faq => faq.status === 1)
        .map(faq => {
          return {
            label: faq.question,
            val: faq.id
          };
        });
    }
  };