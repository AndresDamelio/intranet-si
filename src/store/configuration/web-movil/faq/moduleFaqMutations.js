export default {
  ADD_ITEM(state, item) {
    state.faqs.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.faqs.findIndex(index => index.id == item.id);
    Object.assign(state.faqs[index], item);
  },

  SET_ITEMS(state, items) {
    state.faqs = items;
  },

  SET_ITEM(state, item) {
    state.faq = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.faqs.findIndex(index => index.id == id);
    state.faqs.splice(index, 1);
  }
};
