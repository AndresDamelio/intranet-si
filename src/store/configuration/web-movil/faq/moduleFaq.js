import state from "./moduleFaqState.js";
import mutations from "./moduleFaqMutations.js";
import actions from "./moduleFaqActions.js";
import getters from "./moduleFaqGetters.js";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
