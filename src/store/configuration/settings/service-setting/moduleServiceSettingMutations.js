export default {
  ADD_ITEM(state, item) {
    state.serviceConfigs.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.serviceConfigs.findIndex(index => index.id == item.id);
    state.serviceConfigs[index] = item;
  },

  SET_ITEMS(state, items) {
    state.serviceConfigs = items;
  },

  SET_ITEM(state, item) {
    state.serviceConfig = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.serviceConfigs.findIndex(index => index.id == id);
    state.serviceConfigs.splice(index, 1);
  }
};
