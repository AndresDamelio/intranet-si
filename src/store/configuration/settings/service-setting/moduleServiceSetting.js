import state from "./moduleServiceSettingState.js";
import mutations from "./moduleServiceSettingMutations.js";
import actions from "./moduleServiceSettingActions.js";
import getters from "./moduleServiceSettingGetters.js";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
