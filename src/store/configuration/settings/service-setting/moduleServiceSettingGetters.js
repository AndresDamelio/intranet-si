export default {
  getCustomItems: state => {
    return state.serviceConfigs
      .filter(serviceConfig => serviceConfig.status === 1)
      .map(serviceConfig => {
        return {
          label: serviceConfig.name,
          val: serviceConfig.id,
          eventual: serviceConfig.eventual,
        };
      });
  }
};
