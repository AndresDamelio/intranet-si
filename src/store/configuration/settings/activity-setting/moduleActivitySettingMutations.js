export default {
  ADD_ITEM(state, item) {
    state.activityConfigs.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.activityConfigs.findIndex(index => index.id == item.id);
    state.activityConfigs[index] = item;
  },

  SET_ITEMS(state, items) {
    state.activityConfigs = items;
  },

  SET_ITEM(state, item) {
    state.activityConfig = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.activityConfigs.findIndex(index => index.id == id);
    state.activityConfigs.splice(index, 1);
  }
};
