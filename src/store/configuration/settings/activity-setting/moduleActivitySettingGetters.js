export default {
  getCustomItems: state => {
    return state.activityConfigs
      .filter(activityConfig => activityConfig.status === 1)
      .map(activityConfig => {
        return {
          label: activityConfig.name,
          val: activityConfig.id,
          eventual: activityConfig.eventual,
        };
      });
  }
};
