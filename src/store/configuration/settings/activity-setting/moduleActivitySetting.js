import state from "./moduleActivitySettingState.js";
import mutations from "./moduleActivitySettingMutations.js";
import actions from "./moduleActivitySettingActions.js";
import getters from "./moduleActivitySettingGetters.js";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
