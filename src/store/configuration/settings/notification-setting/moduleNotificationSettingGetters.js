export default {
    getChannel: state => {
        return state.channels.map(channel => {
          return {
            label: channel.publicName,
            val: channel.name
          };
        });
    }
};
