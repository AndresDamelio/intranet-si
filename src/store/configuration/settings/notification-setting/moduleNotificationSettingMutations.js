export default {
  ADD_ITEM(state, item) {
    state.notificationConfigs.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.notificationConfigs.findIndex(index => index.id == item.id);
    state.notificationConfigs[index] = item;
  },

  SET_ITEMS(state, items) {
    state.notificationConfigs = items;
  },

  SET_ITEM(state, item) {
    state.notificationConfig = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.notificationConfigs.findIndex(index => index.id == id);
    state.notificationConfigs.splice(index, 1);
  }
};
