import state from "./moduleNotificationSettingState.js";
import mutations from "./moduleNotificationSettingMutations.js";
import actions from "./moduleNotificationSettingActions.js";
import getters from "./moduleNotificationSettingGetters.js";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
