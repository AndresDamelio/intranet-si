export default {
  getViewValue: state => {
    return state.viewCreate;
  },

  getViewEdit: state => {
    return state.viewDetail;
  },

  getMode: state => {
    return state.modeEdite;
  }
};
