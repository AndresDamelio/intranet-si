import state from "./moduleTelephoneState.js";
import mutations from "./moduleTelephoneMutations.js";
import actions from "./moduleTelephoneActions.js";
import getters from "./moduleTelephoneGetters.js";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
