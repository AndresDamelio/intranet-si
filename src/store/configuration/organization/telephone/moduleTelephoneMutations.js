export default {
  ADD_ITEM(state, item) {
    state.telephones.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.telephones.findIndex(index => index.id == item.id);
    Object.assign(state.telephones[index], item);
  },

  SET_ITEMS(state, items) {
    state.telephones = items;
  },

  SET_ITEM(state, item) {
    state.telephone = item;
  },

  CHANGE_VIEW(state, value) {
    state.viewCreate = value;
  },

  DETAIL_VIEW(state, value) {
    state.viewDetail = value;
  },

  MODE_EDIT(state, value) {
    state.modeEdite = value;
  },

  DELETE_ITEM(state, id) {
    let index = state.telephones.findIndex(index => index.id == id);
    state.telephones.splice(index, 1);
  }
};
