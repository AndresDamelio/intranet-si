
import Vue from "vue";

export default {

  getItems({ commit }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http.get('organizations')
        .then( ( resp ) => {
          commit('SET_ITEM', resp.body.organizations[0]);
          if( resp.body.organizations.length ) {
            commit('THERE_ORGANIZATION', true);
          } else {
            commit('THERE_ORGANIZATION', false);
          }
          resolve(resp.body);
        })
        .catch( ( err ) => {
          reject(err.body);
        })
    })
  },

  getItem({ commit }, id) {
    return new Promise(async (resolve, reject) => {
      await Vue.http.get(`organizations/${id}`)
        .then( ( resp ) => {
          commit('SET_ITEM', resp.body.organization);
          resolve(resp.body);
        })
        .catch( ( err ) => {
          reject(err.body);
        })
    })
  },
}
