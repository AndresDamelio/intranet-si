export default {
  ADD_ITEM(state, item) {
    state.organization = item;
  },

  UPDATE_ITEM(state, item) {
    state.organization = item;
  },

  SET_ITEM(state, item) {
    state.organization = item;
  },

  MODE_EDIT(state, value) {
    state.modeEdite = value;
  },

  UPDATE_IMAGE(state, item) {
    state.organization.logo = item.logo;
  },

  THERE_ORGANIZATION(state, value) {
    state.thereOrganization = value;
  }
};
