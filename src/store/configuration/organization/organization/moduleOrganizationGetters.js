export default {

  getOrganization: state => {
    return state.organization;
  },

  getMode: state => {
    return state.modeEdite;
  },

  thereOrganization: state => {
    return state.thereOrganization;
  },

  getOrganizationID: state => {
    return state.organization.id;
  }
};
