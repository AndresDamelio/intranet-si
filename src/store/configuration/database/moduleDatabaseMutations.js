export default {
  ADD_ITEM(state, item) {
    state.backups.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.backups.findIndex(index => index.id == item.id);
    Object.assign(state.backups[index], item);
  },

  SET_ITEMS(state, items) {
    state.backups = items;
  },

  SET_ITEM(state, item) {
    state.backup = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.backups.findIndex(index => index.id == id);
    state.backups.splice(index, 1);
  },
};
