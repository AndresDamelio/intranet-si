import state from "./moduleDatabaseState";
import mutations from "./moduleDatabaseMutations";
import actions from "./moduleDatabaseActions";
import getters from "./moduleDatabaseGetters";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};