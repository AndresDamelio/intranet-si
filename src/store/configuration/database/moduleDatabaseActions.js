import Vue from "vue";

const getFormData = object => {
  return Object.keys(object).reduce((formData, key) => {
    formData.append(key,Array.isArray(object[key]) ? JSON.stringify(object[key]) : object[key]);
    return formData;
  }, new FormData());
};

export default {
  createBackup({ dispatch }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .get("database/backup")
        .then(async resp => {
          await dispatch("getBackups")
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  restoreUpload( context , { payload }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post("database/restoreUpload",getFormData(payload))
        .then(resp => {
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  restoreName( context , { filename }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .get(`database/restore/${filename}`, )
        .then(resp => {
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  getBackups({ commit }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .get("database/backups")
        .then(resp => {
          commit(`SET_ITEMS`, resp.body.backups);
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },


  downloadBackup( context , {filename}){
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .get(`database/download/${filename}`, {responseType: 'arraybuffer'})
        .then(resp => {
          resolve(resp);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  clean() {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .get("database/clean")
        .then(resp => {
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

}