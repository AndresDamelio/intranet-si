export default {
  setReadNotifications({ commit }) {
    commit(`SET_READ_NOTIFICATIONS`, 2);
  }
};
