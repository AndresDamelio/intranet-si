export default {
  ADD_ITEM(state, item) {
    state.notifications.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.notifications.findIndex(index => index.id == item.id);
    Object.assign(state.notifications[index], item);
  },

  SET_ITEMS(state, items) {
    state.notifications = items;
  },

  SET_ITEM(state, item) {
    state.notification = item;
  },

  SET_READ_NOTIFICATIONS(state, status) {
    state.notifications.forEach(n => {
      if (n.status === 1) n.status = status;
    });
  },

  DELETE_ITEM(state, id) {
    let index = state.notifications.findIndex(index => index.id == id);
    state.notifications.splice(index, 1);
  }
};
