import moment from "moment";

export default {
  getUserCheck: state => filter => {
    let validTypes = filter.types;

    let validFunctions = filter.functions;

    let users = state.users;

    /* Filtro de usuario por tipo(s) */
    function checkType(item) {
      return item.userTypes.filter(el => {
        return validTypes.indexOf(el.name) !== -1;
      }).length;
    }

    /* Filtro de usuario por funcione(s) */
    function checkFunction(item) {
      return item.roles
        .map(r => r.functions.map(f => f.code))
        .flat()
        .filter(el => {
          return validFunctions.indexOf(el) !== -1;
        }).length;
    }

    if (users.length) {
      if (validTypes) {
        users = users.filter(checkType);
      }
      if (validFunctions) {
        users = users.filter(checkFunction);
      }
    }
    return users.map(user => {
      return {
        id: user.id,
        name: user.name,
        lastname: user.lastname,
        specialities: user.specialities.map(s => s.name),
        label: user.name + " " + user.lastname
      };
    });
  },

  getCustomUser: state => filters => {
    let specialities = filters.specialities;
    let functions = filters.functions;
    let users = state.users;

    function checkFunction(item) {
      return item.roles
        .map(r => r.functions.map(f => f.code))
        .flat()
        .filter(el => {
          return functions.indexOf(el) !== -1;
        }).length;
    }

    const userType = user =>
      user.name.toLocaleLowerCase() == "personal" ||
      user.name.toLocaleLowerCase() == "voluntario";

    users = users.filter(user => user.userTypes.some(userType));

    if (specialities) {
      const userSpeciality = specitality =>
        specialities.includes(specitality.name);

      users = users.filter(user => user.specialities.some(userSpeciality));
    }

    if (functions) {
      users = users.filter(checkFunction);
    }

    return users.map(user => {
      return {
        val: user.id,
        label: user.name + " " + user.lastname,
        name: user.name,
        lastname: user.lastname,
        specialities: user.specialities.map(speciality => speciality.name)
      };
    });
  },

  getPatients: state => filter => {

    const userType = user => user.name.toLocaleLowerCase() == "paciente";

    let users = state.users.filter(user => user.userTypes.some(userType));

    if (filter.gender && filter.gender != "null") {
      users = users.filter(user => user.gender == filter.gender);
    }

    if (filter.age) {
      const ageMin = filter.age[0];
      const ageMax = filter.age[1];

      users = users.filter(user => {
        const age = moment().diff(
          moment(user.birthdate, "YYYY/MM/DD"),
          "years"
        );

        return age >= ageMin && age <= ageMax;
      });
    }

    if (filter.phase && filter.phase != "null") {
      users = users.filter(user => user.record.phase == filter.phase);
    }

    return users.map(user => {
      return {
        val: user.id,
        label: user.name + " " + user.lastname,
        record: user.record
      };
    });
  }
};
