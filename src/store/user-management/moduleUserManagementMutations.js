export default {
  SET_USERS(state, users) {
    state.users = users;
  },

  REMOVE_RECORD(state, itemId) {
    const userIndex = state.users.findIndex(u => u.id == itemId);
    state.users.splice(userIndex, 1);
  },

  ADD_ITEM(state, item) {
    state.users.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.users.findIndex(index => index.id == item.id);
    state.users[index] = item;
  },

  SET_ITEMS(state, items) {
    state.users = items;
  },

  SET_ITEM(state, item) {
    state.user = item;
  },

  DELETE_ITEM(state, id) {
    let index = state.users.findIndex(index => index.id == id);
    state.users.splice(index, 1);
  }
};
