export default store => {
  const ability = store.getters["auth/ability"];
  ability.update([store.state.auth.rules]);

  return store.subscribe(mutation => {
    switch (mutation.type) {
      case "auth/LOGIN":
        ability.update([mutation.payload.rules]);
        break;
      case "auth/LOGOUT":
        ability.update([{ actions: "F-ALL-0", subject: "userNotLogged" }]);
        break;
    }
  });
};
