import state from "./moduleRecordState";
import actions from "./moduleRecordActions";
import getters from "./moduleRecordGetters";
import mutations from "./moduleRecordMutations";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
