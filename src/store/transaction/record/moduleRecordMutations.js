export default {
  UPDATE_ITEM(state, item) {
    let index = state.records.findIndex(index => index.id == item.id);
    state.records[index] = item;
  },
  SET_ITEMS(state, items) {
    state.records = items;
  },

  SET_ITEM(state, item) {
    state.record = item;
  }
};
