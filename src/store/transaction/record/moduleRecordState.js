export default {
  record: null,
  records: [{
    id: 1,
    user: {
      name: "Ana",
      lastname: "Hernandez",
      birthdate: "1995/11/20",
      gender: "F",
      phone: "1234567",
      email: "anacec@gmail.com",
      image: "vaic4fh59xssspigos5x"
    },
    code: "A001",
    date: "2020/06/29",
  },
  {
    id: 2,
    user: {
      name: "Jose",
      lastname: "D Amelio",
      birthdate: "1996/08/24",
      gender: "F",
      phone: "1234567",
      email: "anacec@gmail.com"
    },
    code: "A002",
    date: "2020/06/29",
  }
]
}