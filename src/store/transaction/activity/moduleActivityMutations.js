export default {

  ADD_ITEM(state, item) {
    state.activities.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.activities.findIndex(index => index.id == item.id);
    Object.assign(state.activities[index], item);
  },

  SET_ITEMS(state, items) {
    state.activities = items;
  },

  SET_ITEM(state, item) {
    state.activity = item;
  },

  SET_REPLANNINGS(state, replannings) {
    state.replannings = replannings;
  },

  SET_RESULTS(state, results) {
    state.results = results;
  },

  SET_INCIDENCE(state, incidence) {
    state.incidences.push(incidence);
  },

  SET_INCIDENCES(state, incidences) {
    state.incidences = incidences;
  },

  DELETE_INCIDENCE(state, id) {
    let index = state.incidences.findIndex(index => index.id == id);
    state.incidences.splice(index, 1);
  }
};
