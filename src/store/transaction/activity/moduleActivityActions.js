import Vue from 'vue';

export default {
  replanning(context, payload) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post("replannings/activities", payload)
        .then(resp => {
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  getReplannings({ commit }, id) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .get(`activities/${id}`)
        .then(resp => {
          commit("SET_REPLANNINGS", resp.body.activity.replannings);
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  registerResult({ commit }, payload) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post(`activities/${payload.id}/registerResult`, payload)
        .then(resp => {
          commit("SET_RESULTS", resp.body.activity.results);
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  getResults({ commit }, id) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .get(`activities/${id}`)
        .then(resp => {
          commit("SET_RESULTS", resp.body.activity.results);
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  getIncidences({ commit }, id) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .get(`incidences/activities/${id}`)
        .then(resp => {
          commit("SET_INCIDENCES", resp.body.incidences);
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  }
};
