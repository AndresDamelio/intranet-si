export default {
  getReplannings: state => {
    return state.replannings;
  },

  getResults: state => {
    return state.results;
  },

  getIncidences: state => {
    return state.incidences;
  }
};
