import state from "./moduleActivityState";
import actions from "./moduleActivityActions";
import getters from "./moduleActivityGetters";
import mutations from "./moduleActivityMutations";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
