import state from "./moduleRequestState";
import actions from "./moduleRequestActions";
import getters from "./moduleRequestGetters";
import mutations from "./moduleRequestMutations";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
