import Vue from "vue";

export default {
  reject({ commit }, { id, reasonRejectionId }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post(`requests/${id}/reject`, { reasonRejectionId },{
          headers: {
            'modelName': 'request',
            'route':'requests',
            'id': id.toString(),
            'operation': 'reject',
            'from': 'desktop',
          }
        })
        .then(resp => {
          commit(`UPDATE_ITEM`, resp.body.request);
          commit(`SET_ITEM`, resp.body.request);
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },
  rejectRequest({ commit }, { id, reasonRejectionId, req }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post(`requests/${id}/${req}`, { reasonRejectionId })
        .then(resp => {
          commit(`UPDATE_ITEM`, resp.body.request);
          commit(`SET_ITEM`, resp.body.request);
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },
  approveRequest({ commit }, { id, payload, req }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post(`requests/${id}/${req}`,  payload )
        .then(resp => {
          commit(`APPROVE_REQUEST`, id);
          commit(`SET_ITEM`, resp.body.request);
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },
  approve({ commit }, { id, payload }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post(`requests/${id}/approve`, payload,{
          headers: {
            'modelName': 'request',
            'route':'requests',
            'id': id.toString(),
            'operation': 'approve',
            'from': 'desktop',
          }
        })
        .then(resp => {
          commit(`APPROVE_REQUEST`, id);
          commit(`appointment/SET_ITEM`, resp.body.appointment, { root: true });
          commit(`appointment/ADD_ITEM`, resp.body.appointment, { root: true });
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },
  sendRequest({ commit }, { data, req}) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post(`requests/${req}`, data)
        .then(resp => {
          commit(`request/SET_ITEM`, resp.body['request'], { root: true });
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },
   
};
