export default {

  UPDATE_ITEM(state, item) {
    let index = state.requests.findIndex(index => index.id == item.id);
    Object.assign(state.requests[index], item);
  },
  APPROVE_REQUEST(state,id){
    let index = state.requests.findIndex(index => index.id == id);
    state.requests[index].status = "APPROVED";
  },
  REJECT_REQUEST(state,id){
    let index = state.requests.findIndex(index => index.id == id);
    state.requests[index].status = "REJECT";
  },

  SET_ITEMS(state, items) {
    state.requests = items;
  },

  SET_ITEM(state, item) {
    state.request = item;
  },

/*   DELETE_ITEM(state, id) {
    let index = state.roles.findIndex(index => index.id == id);
    state.roles.splice(index, 1);
  } */
};
