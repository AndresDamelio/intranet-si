export default {
  ADD_INCIDENT(state, { items, item }) {
    state[items].push(item);
  },

  SET_ITEMS(state, { item, values }) {
    state[item] = values;
  },

  SET_ITEM(state, { item, value }) {
    state[item] = value;
  }
};
