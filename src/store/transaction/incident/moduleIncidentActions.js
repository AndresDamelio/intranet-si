import Vue from "vue";

export default {
  create({ commit }, { payload, type }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post('incidences', payload)
        .then(resp => {
          if (Object.is(type, "service")) {
            commit("service/SET_INCIDENCE", resp.body.incidence, { root: true });
          } else {
            commit("activity/SET_INCIDENCE", resp.body.incidence, { root: true });
          }
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  delete({ commit }, { id, type }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .delete(`incidences/${id}/hard`)
        .then(resp => {
          if (Object.is(type, "service")) {
            commit("service/DELETE_INCIDENCE", resp.body.incidence.id, { root: true });
          } else {
            commit("activity/DELETE_INCIDENCE", resp.body.incidence.id, { root: true });
          }
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  getItems({ commit }, { id, route, type, property }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .get(`${route}/${type}/${id}`)
        .then(resp => {
          if (Object.is(type, "service")) {
            commit("SET_ITEMS", {
              item: property,
              values: resp.body.incidents
            });
          } else {
            commit("SET_ITEMS", {
              item: property,
              values: resp.body.incidents
            });
          }
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  }
};
