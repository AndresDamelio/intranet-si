import state from "./moduleIncidentState";
import actions from "./moduleIncidentActions";
import getters from "./moduleIncidentGetters";
import mutations from "./moduleIncidentMutations";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
