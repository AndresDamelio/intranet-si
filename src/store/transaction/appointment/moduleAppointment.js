import state from "./moduleAppointmentState";
import actions from "./moduleAppointmentActions";
import getters from "./moduleAppointmentGetters";
import mutations from "./moduleAppointmentMutations";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
