import Vue from "vue";

export default {
  attend(context, { id, payload }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post(`appointments/${id}/${payload.result == "ACCEPTED"? 'approve' : 'reject'}`,  payload,{
          headers: {
            'modelName': 'appointment',
            'route':'appointments',
            'id': id.toString(),
            'operation': 'attend',
            'from': 'desktop',
          }
        } )
        .then(resp => {
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },
  replanning({ commit }, { id, payload }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post(`appointments/${id}/replanning`, { payload },{
          headers: {
            'modelName': 'appointment',
            'route':'appointments',
            'id': id.toString(),
            'operation': 'replanning',
            'from': 'desktop',
          }
        })
        .then(resp => {
          commit('UPDATE_ITEM', resp.body.appointment);
          commit('SET_ITEM', resp.body.appointment);
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },
};