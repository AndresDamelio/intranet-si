export default {

  UPDATE_ITEM(state, item) {
    let index = state.appointments.findIndex(index => index.id == item.id);
    Object.assign(state.appointments[index], item);
  },
  ADD_ITEM(state, item){
    state.appointments.push(item);
  },

  SET_ITEMS(state, items) {
    state.appointments = items;
  },

  SET_ITEM(state, item) {
    state.appointment = item;
  },
};
