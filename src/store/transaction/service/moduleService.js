import state from "./moduleServiceState";
import actions from "./moduleServiceActions";
import getters from "./moduleServiceGetters";
import mutations from "./moduleServiceMutations";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
