export default {
  ADD_ITEM(state, item) {
    state.services.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.services.findIndex(index => index.id == item.id);
    Object.assign(state.services[index], item);
  },

  SET_ITEMS(state, items) {
    state.services = items;
  },

  SET_ITEM(state, item) {
    state.service = item;
  },

  UPDATE_PATIENTS(state, patients) {
    state.service["otherUsers"] = patients;
  },

  SET_REPLANNINGS(state, replannings) {
    state.replannings = replannings;
  },

  SET_RESULTS(state, results) {
    state.results = results;
  },

  SET_INCIDENCE(state, incidence) {
    state.incidences.push(incidence);
  },

  SET_INCIDENCES(state, incidences) {
    state.incidences = incidences;
  },

  DELETE_INCIDENCE(state, id) {
    let index = state.incidences.findIndex(index => index.id == id);
    state.incidences.splice(index, 1);
  }
};
