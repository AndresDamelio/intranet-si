import Vue from "vue";

export default {
  enroll({ commit }, payload) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post("services/users/inscribe", payload)
        .then(resp => {
          commit("UPDATE_PATIENTS", resp.body.service.otherUsers);
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  replanning(context, payload) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post("replannings/services", payload)
        .then(resp => {
          //commit("UPDATE_PATIENTS", resp.body.service.otherUsers);
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  getReplannings({ commit }, id) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .get(`services/${id}`)
        .then(resp => {
          commit("SET_REPLANNINGS", resp.body.service.replannings);
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  registerResult({ commit }, payload) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post(`services/${payload.id}/registerResult`, payload)
        .then(resp => {
          commit("SET_RESULTS", resp.body.service.results);
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  getResults({ commit }, id) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .get(`services/${id}/getResults`)
        .then(resp => {
          commit("SET_RESULTS", resp.body.results);
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  getIncidences({ commit }, id) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .get(`incidences/services/${id}`)
        .then(resp => {
          commit("SET_INCIDENCES", resp.body.incidences);
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  }
};
