export default {
  getUsers: state => {
    let responsibles = state.service.responsibles.map(user => {
      return { label: user.name + " " + user.lastname, val: user.id };
    });

    let otherUsers = state.service.otherUsers.map(user => {
      return { label: user.name + " " + user.lastname, val: user.id };
    });

    return ["Responsables", ...responsibles, "Pacientes", ...otherUsers];
  },

  getReplannings: state => {
    return state.replannings;
  },

  getResults: state => {
    return state.results;
  },

  getIncidences: state => {
    return state.incidences;
  }
};
