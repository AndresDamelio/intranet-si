import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";
import abilityPlugin from "./ability";

Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
});

import state from "./state";
import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";
import moduleAuth from "./auth/moduleAuth.js";
import moduleGlobal from "./global-module/moduleGlobal.js";
import moduleActivityType from "./configuration/basic-data/activity-type/moduleActivityType.js";
import moduleCenterType from "./configuration/basic-data/center-type/moduleCenterType";
import moduleMessageType from "./configuration/basic-data/message-type/moduleMessageType";
import moduleTelephoneType from "./configuration/basic-data/telephone-type/moduleTelephoneType";
import moduleNotificationType from "./configuration/basic-data/notification-type/moduleNotificationType";
import moduleCategory from "./configuration/basic-data/category/moduleCategory.js";
import modulePathology from "./configuration/basic-data/pathology/modulePathology.js";
import moduleRole from "./configuration/basic-data/role/moduleRole.js";
import moduleSpeciality from "./configuration/basic-data/speciality/moduleSpeciality.js";
import moduleServiceType from "./configuration/basic-data/service-type/moduleServiceType.js";
import moduleReasonRejection from "./configuration/basic-data/reason-rejection/moduleReasonRejection.js";
import modulePosition from "./configuration/basic-data/position/modulePosition.js";
import modulePlace from "./configuration/basic-data/place/modulePlace.js";
import moduleResourceType from "./configuration/basic-data/resource-type/moduleResourceType.js";
import moduleSchedule from "./configuration/basic-data/schedule/moduleSchedule.js";
import moduleParam from "./configuration/basic-data/param/moduleParam.js";
import moduleResource from "./configuration/basic-data/resource/moduleResource.js";
import moduleQuestion from "./configuration/basic-data/question/moduleQuestion.js";
import moduleUserType from "./configuration/basic-data/user-type/moduleUserType.js";
import moduleBlock from "./configuration/basic-data/block/moduleBlock.js";
import moduleNotificationSetting from "./configuration/settings/notification-setting/moduleNotificationSetting.js";
import moduleServiceSetting from "./configuration/settings/service-setting/moduleServiceSetting.js";
import moduleActivitySetting from "./configuration/settings/activity-setting/moduleActivitySetting.js";
import moduleCenter from "./configuration/web-movil/center/moduleCenter.js";
import moduleDatabase from "./configuration/database/moduleDatabase.js";
import moduleElement from "./configuration/web-movil/element/moduleElement";
import moduleConfigWebMobile from "./configuration/web-movil/config/moduleConfig";
import moduleFaq from "./configuration/web-movil/faq/moduleFaq.js";
import modulePost from "./configuration/web-movil/post/modulePost.js";
import moduleTelephone from "./configuration/organization/telephone/moduleTelephone.js";
import moduleOrganization from "./configuration/organization/organization/moduleOrganization.js";
import moduleRequest from "./transaction/request/moduleRequest.js";
import moduleUserManagement from "./user-management/moduleUserManagement.js";
import moduleUserLog from "./user-log/moduleUserLog.js";
import moduleAppointment from "./transaction/appointment/moduleAppointment.js";
import moduleRecord from "./transaction/record/moduleRecord.js";
import moduleMessage from "./message/moduleMessage.js";
import moduleService from "./transaction/service/moduleService.js";
import moduleActivity from "./transaction/activity/moduleActivity.js";
import moduleIncident from "./transaction/incident/moduleIncident.js";
import moduleNotification from "./notification/moduleNotification.js";
import moduleCalendar from "./calendar/moduleCalendar.js";

export default new Vuex.Store({
  getters,
  mutations,
  state,
  actions,
  modules: {
    auth: moduleAuth,
    activityType: moduleActivityType,
    category: moduleCategory,
    centerType: moduleCenterType,
    messageType: moduleMessageType,
    notificationType: moduleNotificationType,
    pathology: modulePathology,
    telephoneType: moduleTelephoneType,
    telephone: moduleTelephone,
    role: moduleRole,
    speciality: moduleSpeciality,
    serviceType: moduleServiceType,
    reasonRejection: moduleReasonRejection,
    position: modulePosition,
    place: modulePlace,
    resourceType: moduleResourceType,
    faq: moduleFaq,
    organization: moduleOrganization,
    param: moduleParam,
    resource: moduleResource,
    schedule: moduleSchedule,
    global: moduleGlobal,
    activityConfig: moduleActivitySetting,
    serviceConfig: moduleServiceSetting,
    question: moduleQuestion,
    userType: moduleUserType,
    request: moduleRequest,
    center: moduleCenter,
    element: moduleElement,
    configWebMobile: moduleConfigWebMobile,
    user: moduleUserManagement,
    database: moduleDatabase,
    block: moduleBlock,
    post: modulePost,
    appointment: moduleAppointment,
    record: moduleRecord,
    message: moduleMessage,
    notificationConfig: moduleNotificationSetting,
    userLog: moduleUserLog,
    service: moduleService,
    activity: moduleActivity,
    incident: moduleIncident,
    notification: moduleNotification,
    calendar: moduleCalendar
  },
  plugins: [vuexLocal.plugin, abilityPlugin],
  strict: process.env.NODE_ENV !== "production"
});
