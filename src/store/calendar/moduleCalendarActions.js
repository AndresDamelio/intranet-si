import Vue from "vue";
import { getEventArray } from "@/utils/formatEventData";

const eventLabels = [
  { text: "Servicio", value: "service", color: "success" },
  { text: "Actividad", value: "activity", color: "warning" },
  { text: "Cita", value: "appointment", color: "danger" }
];

export default {
  setEventLabels({ commit }) {
    commit("SET_EVENT_LABELS", eventLabels);
  },

  fetchEvents({ commit }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .get("users/diary/all")
        .then(resp => {
          commit(
            `SET_ITEMS`,
            getEventArray(
              resp.body["diary"]["appointments"],
              resp.body["diary"]["activities"],
              resp.body["diary"]["services"]
            )
          );
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  }
};
