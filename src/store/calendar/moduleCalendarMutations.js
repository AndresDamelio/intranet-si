export default {
  SET_ITEMS(state, items) {
    state.events = items;
  },

  SET_ITEM(state, item) {
    state.event = item;
  },

  SET_EVENT_LABELS(state, items) {
    state.eventLabels = items;
  }
};
