
export default {
  user: null,
  rules: {},
  isAuthenticated: false,
  isUserLoggedIn: () => {
    let isAuthenticated = false;

    return localStorage.getItem("userInfo") && isAuthenticated;
  }
};
