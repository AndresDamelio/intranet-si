import { Ability } from '@casl/ability'

export default {
  getUserAuth: (state) => {
    return state.user;
  },
  ability() {
    return new Ability([], {
      subjectName(subject) {
        return !subject
          ? subject
          : "userNotLogged"
      }
    })
  }
};
