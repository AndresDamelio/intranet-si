export default {
  AUTH_SUCCESS(state, user) {
    state.user = user;
  },

  LOGIN(state, payload) {
    state.isAuthenticated = true;
    window.localStorage.setItem("access_token", payload.token);
  },

  LOGOUT(state) {
    state.isAuthenticated = false;
    state.user = null;
    state.rules = {};
    window.localStorage.removeItem("access_token");
  },

  UPDATE_PROFILE(state, user) {
    if(user.id == state.user.id){
      state.user.name = user.name;
      state.user.lastname = user.lastname;
      state.user.image = user.image;
      state.user.birthdate = user.birthdate;
      state.user.gender = user.gender;
      state.user.telephone = user.telephone;
      state.user.specialities = user.specialities;
      state.user.roles = user.roles;
      state.user.position = user.position;
      state.user.userTypes = user.userTypes;
    }
  },

  SET_RULES(state, rules) {
    state.rules = rules;
  }
};
