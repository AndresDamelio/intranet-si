import Vue from "vue";
import router from "@/router";

const getFormData = object => {
  return Object.keys(object).reduce((formData, key) => {
    formData.append(
      key,
      Array.isArray(object[key]) ? JSON.stringify(object[key]) : object[key]
    );
    return formData;
  }, new FormData());
};

export default {
  updateProfile({ commit }, user) {
    commit("UPDATE_PROFILE", user);
  },

  login({ commit }, payload) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post("authentication/login", getFormData(payload))
        .then(async resp => {
          const user = await resp.body.user;
          const functions = await user.functions.map(fun => fun.code);
          const rules = { actions: functions, subject: "userLogged" };
          const token = await resp.body.sessionToken;
          commit("SET_RULES", rules);
          commit("AUTH_SUCCESS", user);
          commit("LOGIN", { token: token, rules: rules });
          router.push("/");
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  logout({ commit }) {
    return new Promise(resolve => {
      commit("LOGOUT");
      router.push("/login");
      resolve();
    });
  },

  recoverPassword(context, email) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post("authentication/recover", getFormData({ email: email }))
        .then(resp => {
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  changePassword(context, payload) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .put("authentication/changePassword", payload)
        .then(resp => {
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  getUserLogged({ commit }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post("authentication/loggedInUser")
        .then(async resp => {
          const user = await resp.body.user;
          const functions = await user.functions.map(fun => fun.code);
          const rules = { actions: functions, subject: "userLogged" };
          const token = await resp.body.sessionToken;
          commit("SET_RULES", rules);
          commit("LOGIN", { token: token, rules: rules });
          commit("AUTH_SUCCESS", user);
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },
};
