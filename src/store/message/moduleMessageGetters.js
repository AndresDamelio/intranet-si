export default {
  filteredMessages: state =>
    state.contactMessages.filter(message => {
      return (
        state.message_filter === message.messageType &&
        (message.name
          .toLowerCase()
          .includes(state.messageSearchQuery.toLowerCase()) ||
          message.lastname
            .toLowerCase()
            .includes(state.messageSearchQuery.toLowerCase()) ||
          message.email
            .toLowerCase()
            .includes(state.messageSearchQuery.toLowerCase()))
      );
    }),
  getMessage: state => messageId =>
    state.contactMessages.find(message => message.id == messageId)
};
