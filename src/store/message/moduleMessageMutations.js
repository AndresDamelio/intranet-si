export default {
  ADD_ITEM(state, item) {
    state.contactMessages.push(item);
  },

  UPDATE_ITEM(state, item) {
    let index = state.contactMessages.findIndex(index => index.id == item.id);
    Object.assign(state.contactMessages[index], item);
  },

  UPDATE_MESSAGE_FILTER(state, urlParam) {
    state.message_filter = urlParam;
  },

  SET_ITEMS(state, items) {
    state.contactMessages = items;
  },

  SET_ITEM(state, item) {
    state.contactMessage = item;
  },

  SET_MESSAGE_SEARCH_QUERY(state, query) {
    state.messageSearchQuery = query;
  },

  DELETE_ITEM(state, id) {
    let index = state.contactMessages.findIndex(index => index.id == id);
    state.contactMessages.splice(index, 1);
  }
};
