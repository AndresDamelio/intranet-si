import Vue from "vue";

export default {
  setMessageSearchQuery({ commit }, query) {
    commit("SET_MESSAGE_SEARCH_QUERY", query);
  },
  reply({ commit }, { payload, id }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post(`messages/${id}/reply`, payload)
        .then(resp => {
          commit(`message/UPDATE_ITEM`, resp.body["contactMessage"], {
            root: true
          });
          commit(`message/SET_ITEM`, resp.body["contactMessage"], {
            root: true
          });
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },
  delete({ commit, rootState }, { id, route, module, data }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .delete(`${route}/${id}`)
        .then(resp => {
          if (Object.is(resp.status, 201)) {
            commit(`${module}/UPDATE_ITEM`, resp.body[data], { root: true });
            if (rootState[module][data]) {
              commit(`${module}/SET_ITEM`, resp.body[data], { root: true });
            }
          } else {
            commit(`${module}/DELETE_ITEM`, resp.body[data].id, { root: true });
            if (rootState[module][data]) {
              commit(`${module}/SET_ITEM`, null, { root: true });
            }
          }
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  }
};
