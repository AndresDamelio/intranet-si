import { models } from "@/fake-db/data/basic/models.js";
import moment from "moment";
export default {
  models,
  enitities:[
    {
      name: 'center',
      publicName: 'centro'
    },
    {
      name: 'element',
      publicName: 'elemento'
    },
    {
      name: 'configWebMobile',
      publicName: 'configuracion web móvil'
    },
    {
      name: 'post',
      publicName: 'publicación'
    },
    {
      name: 'faq',
      publicName: 'pregunta frecuente'
    },
    {
      name: 'message',
      publicName: 'mensaje'
    },
    {
      name: 'activityConfig',
      publicName: 'configuración de actividad'
    },
    {
      name: 'serviceConfig',
      publicName: 'configuración de servicio'
    },
    {
      name: 'notification',
      publicName: 'notificación'
    },
    {
      name: 'user',
      publicName: 'usuario'
    },
    {
      name: 'record',
      route: 'records',
      module: 'record',
      property: 'records',
      publicName: 'Pacientes',
      report:true,
      fields:[
        {
          name:'age',
          publicName:'Edad',
          type: 'quantity',
        },
        {
          name:'gender',
          publicName:'Género',
          type: 'quality',
          values:[
            {
              name:'F',
              publicName:'Femenino'
            },
            {
              name:'M',
              publicName:'Masculino'
            },
          ]
        },
        {
          name:'phase',
          publicName:'Estadío',
          type: 'quality',
          values:[
            {
              name:'PHASE1',
              publicName:'Estadío I'
            },
            {
              name:'PHASE2',
              publicName:'Estadío II'
            },
            {
              name:'PHASE3',
              publicName:'Estadío III'
            },
            {
              name:'PHASE4',
              publicName:'Estadío IV'
            },
            {
              name:'PHASE5',
              publicName:'Estadío V'
            },
          ]
        },
        {
          name:'maritalStatus',
          publicName:'Estado civil',
          type: 'quality',
          values:[
            {
              name:'Soltero(a)',
              publicName:'Soltero(a)'
            },
            {
              name:'Casado(a)',
              publicName:'Casado(a)'
            },
            {
              name:'Divorciado(a)',
              publicName:'Divorciado(a)'
            },
            {
              name:'Viudo(a)',
              publicName:'Viudo(a)'
            },
          ]
        },
      ],
      columns: [
        {
          headerName: "Número",
          filter: true,
          width: 100,
          valueGetter: function(params) {
            //return  moment(String(params.data.date)).format("dd/mm/yyyy")
            return "EXP-00" + (params.data.id+100).toString().slice(1);
          }
        },
        {
          headerName: "Paciente",
          filter: true,

          valueGetter: function(params) {
            //return  moment(String(params.data.date)).format("dd/mm/yyyy")
            return params.data.user ? params.data.user.name + " " + params.data.user.lastname : "";
          }
        },
        {
          headerName: "Estadío",
          filter: true,
          width: 100,
          valueGetter: function(params) {
            //return  moment(String(params.data.date)).format("dd/mm/yyyy")
            if(params.data.recordDetails && params.data.recordDetails.length){
              switch(params.data.phase){
                case 'PHASE1': return 'Estadío I';
                case 'PHASE2': return 'Estadío II';
                case 'PHASE3': return 'Estadío III';
                case 'PHASE4': return 'Estadío IV';
                case 'PHASE5': return 'Estadío V';
              }
            }
            return "Por definir";
          }
        },
        {
          headerName: "Creado el",
          field: "date",
          filter: "agDateColumnFilter",
          valueGetter: function(params) {
            return  moment(String(params.data.date)).format("DD/MM/YYYY")
          },
          filterParams: {
            comparator: (filterLocalDateAtMidnight, cellValue) => {
              var dateAsString = cellValue;
              if (dateAsString == null) return -1;
              var dateParts = dateAsString.split("/");
              var cellDate = new Date(
                Number(dateParts[2]),
                Number(dateParts[1]) - 1,
                Number(dateParts[0])
              );
              if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                return 0;
              }
              if (cellDate < filterLocalDateAtMidnight) {
                return -1;
              }
              if (cellDate > filterLocalDateAtMidnight) {
                return 1;
              }
            },
            browserDatePicker: true
          },
          width: 150,
          suppressSizeToFit: true
        },
      ]
    },
    {
      name: 'appointment',
      route: 'appointments',
      module: 'appointment',
      property: 'appointments',
      publicName: 'Citas iniciales',
      report:true,
      fields:[
        {
          name:'gender',
          publicName:'Género',
          type: 'quality',
          values:[
            {
              name:'F',
              publicName:'Femenino'
            },
            {
              name:'M',
              publicName:'Masculino'
            },
          ]
        },
        {
          name:'responsibleName',
          publicName:'Responsable',
          type: 'quality',
        },
        {
          name:'reason',
          publicName:'Motivo de rechazo',
          type: 'quality',
        },
        {
          name:'stage',
          publicName:'Estatus',
          type: 'quality',
          values:[
            {
              name:'PENDING',
              publicName:'Pendiente'
            },
            {
              name:'FINISHED',
              publicName:'Atendida'
            },
          ]
        },
      ],
      columns: [
        {
          headerName: "Fecha",
          field: "date",
          filter: "agDateColumnFilter",
          valueGetter: function(params) {
            return  moment(String(params.data.date)).format("DD/MM/YYYY")
          },
          filterParams: {
            comparator: (filterLocalDateAtMidnight, cellValue) => {
              var dateAsString = cellValue;
              if (dateAsString == null) return -1;
              var dateParts = dateAsString.split("/");
              var cellDate = new Date(
                Number(dateParts[2]),
                Number(dateParts[1]) - 1,
                Number(dateParts[0])
              );
              if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                return 0;
              }
              if (cellDate < filterLocalDateAtMidnight) {
                return -1;
              }
              if (cellDate > filterLocalDateAtMidnight) {
                return 1;
              }
            },
            browserDatePicker: true
          },
          width: 150,
          suppressSizeToFit: true
        },
        {
          headerName: "Nombre",
          filter: true,
          valueGetter: function(params) {
            //return  moment(String(params.data.date)).format("dd/mm/yyyy")
            return params.data.request.name + " " + params.data.request.lastname;
          }
        },
        {
          headerName: "Edad (años)",
          field: "birthdate",
          filter: "agNumberColumnFilter",
          valueGetter: function(params) {
            return moment().diff(moment(params.data.request.birthdate, "YYYY/MM/DD"), 'years');
          },
        },
        {
          headerName: "Estado",
          field: "stage",
          filter: true,
          cellRenderer: "statusRenderer",
          cellClass: "cell-status"
        }
      ],
    },
    {
      name: 'service',
      route: 'services',
      module: 'service',
      property: 'services',
      publicName: 'Servicios',
      report:true,
      fields:[
        {
          name:'capacity',
          publicName:'Capacidad',
          type: 'ageDate',
        },
        {
          name:'phaseFor',
          publicName:'Estadío objetivo',
          type: 'quality',
          values:[
            {
              name:'PHASE1',
              publicName:'Estadío I'
            },
            {
              name:'PHASE2',
              publicName:'Estadío II'
            },
            {
              name:'PHASE3',
              publicName:'Estadío III'
            },
            {
              name:'PHASE4',
              publicName:'Estadío IV'
            },
            {
              name:'PHASE5',
              publicName:'Estadío V'
            },
          ]
        },
      ]
    },
    {
      name: 'activity',
      route: 'activities',
      module: 'activity',
      property: 'activities',
      publicName: 'Actividades',
      report:true,
      fields:[
        {
          name:'capacity',
          publicName:'Capacidad',
          type: 'ageDate',
        },
        {
          name:'type',
          publicName:'Tipo',
          type: 'quality',
        },
        {
          name:'phaseFor',
          publicName:'Estadío objetivo',
          type: 'quality',
          values:[
            {
              name:'PHASE1',
              publicName:'Estadío I'
            },
            {
              name:'PHASE2',
              publicName:'Estadío II'
            },
            {
              name:'PHASE3',
              publicName:'Estadío III'
            },
            {
              name:'PHASE4',
              publicName:'Estadío IV'
            },
            {
              name:'PHASE5',
              publicName:'Estadío V'
            },
          ]
        },
      ],
      columns: [
        {
          headerName: "Nombre",
          field: "name",
          filter: true
        },
        {
          headerName: "Capacidad máxima",
          field: "capacity",
          filter: true
        },
        {
          headerName: "Fecha",
          field: "date",
          filter: "agDateColumnFilter",
          valueGetter: function(params) {
            return moment(String(params.data.date)).format("DD/MM/YYYY");
          },
          filterParams: {
            comparator: (filterLocalDateAtMidnight, cellValue) => {
              var dateAsString = cellValue;
              if (dateAsString == null) return -1;
              var dateParts = dateAsString.split("/");
              var cellDate = new Date(
                Number(dateParts[2]),
                Number(dateParts[1]) - 1,
                Number(dateParts[0])
              );
              if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                return 0;
              }
              if (cellDate < filterLocalDateAtMidnight) {
                return -1;
              }
              if (cellDate > filterLocalDateAtMidnight) {
                return 1;
              }
            },
            browserDatePicker: true
          },
          width: 150,
          suppressSizeToFit: true
        },
        {
          headerName: "Estado",
          field: "stage",
          filter: true,
          cellRenderer: "statusRenderer",
          cellClass: "cell-status"
        }
      ],
    },
    {
      name: 'request',
      route: 'requests',
      module: 'request',
      property: 'requests',
      publicName: 'solicitudes',
      report:true,
      fields:[
        {
          name:'age',
          publicName:'Edad',
          type: 'ageDate',
        },
        {
          name:'gender',
          publicName:'Género',
          type: 'quality',
          values:[
            {
              name:'F',
              publicName:'Femenino'
            },
            {
              name:'M',
              publicName:'Masculino'
            },
          ]
        },
        {
          name:'stage',
          publicName:'Estatus',
          type: 'quality',
          values:[
            {
              name:'PENDING',
              publicName:'Pendiente'
            },
            {
              name:'APPROVED',
              publicName:'Aprobado'
            },
            {
              name:'REJECTED',
              publicName:'Rechazado'
            },
          ]
        },
      ],
      columns: [
        {
          headerName: "Fecha",
          field: "date",
          filter: "agDateColumnFilter",
          valueGetter: function(params) {
            return  moment(String(params.data.date)).format("DD/MM/YYYY")
          },
          filterParams: {
            comparator: (filterLocalDateAtMidnight, cellValue) => {
              var dateAsString = cellValue;
              if (dateAsString == null) return -1;
              var dateParts = dateAsString.split("/");
              var cellDate = new Date(
                Number(dateParts[2]),
                Number(dateParts[1]) - 1,
                Number(dateParts[0])
              );
              if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                return 0;
              }
              if (cellDate < filterLocalDateAtMidnight) {
                return -1;
              }
              if (cellDate > filterLocalDateAtMidnight) {
                return 1;
              }
            },
            browserDatePicker: true
          },
          width: 150,
          suppressSizeToFit: true
        },
        {
          headerName: "Nombre",
          filter: true,
          valueGetter: function(params) {
            //return  moment(String(params.data.date)).format("dd/mm/yyyy")
            return params.data.name + " " + params.data.lastname;
          }
        },
        {
          headerName: "Edad (años)",
          field: "birthdate",
          filter: "agNumberColumnFilter",
          valueGetter: function(params) {
            return moment().diff(moment(params.data.birthdate, "YYYY/MM/DD"), 'years');
          },
        },
        {
          headerName: "Estado",
          field: "stage",
          filter: true,
          cellRenderer: "statusRenderer",
          cellClass: "cell-status"
        }
      ],
    },
  ],
};
