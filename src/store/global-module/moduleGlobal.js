import state from "./moduleGlobalState.js";
import mutations from "./moduleGlobalMutations.js";
import actions from "./moduleGlobalActions.js";
import getters from "./moduleGlobalGetters.js";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
