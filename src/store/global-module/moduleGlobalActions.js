import Vue from "vue";

const getFormData = object => {
  return Object.keys(object).reduce((formData, key) => {
    formData.append(
      key,
      Array.isArray(object[key]) ? JSON.stringify(object[key]) : object[key]
    );
    return formData;
  }, new FormData());
};

export default {
  create({ commit }, { payload, route, module, data }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post(route, payload,{
          headers: {
            'modelName': data,
            'route':route,
            'operation': 'create',
            'from': 'desktop',
          }
        })
        .then(resp => {
          commit(`${module}/ADD_ITEM`, resp.body[data], { root: true });
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  createWithImage({ commit }, { payload, route, module, data }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post(route, getFormData(payload),{
          headers: {
            'modelName': data,
            'route':route,
            'operation': 'create',
            'from': 'desktop',
          }
        })
        .then(resp => {
          commit(`${module}/ADD_ITEM`, resp.body[data], { root: true });
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  createSimple(context, { payload, route }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .post(route, payload)
        .then(resp => {
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  getItems({ commit }, { route, module, data }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .get(route)
        .then(resp => {
          commit(`${module}/SET_ITEMS`, resp.body[data], { root: true });
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  getItem({ commit }, { id, route, module, data }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .get(`${route}/${id}`)
        .then(resp => {
          commit(`${module}/SET_ITEM`, resp.body[data], { root: true });
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  update({ commit }, { payload, route, module, data }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .put(`${route}/${payload.id}`, payload, {
          headers: {
            'modelName': data,
            'route':route,
            'id': payload.id.toString(),
            'operation': 'update',
            'from': 'desktop',
          }
        })
        .then(resp => {
          commit(`${module}/UPDATE_ITEM`, resp.body[data], { root: true });
          commit(`${module}/SET_ITEM`, resp.body[data], { root: true });
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  updateWithImage({ commit }, { payload, route, module, data }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .put(`${route}/${payload.id}`, getFormData(payload),{
          headers: {
            'modelName': data,
            'route':route,
            'id': payload.id.toString(),
            'operation': 'update',
            'from': 'desktop',
          }
        })
        .then(resp => {
          commit(`${module}/UPDATE_ITEM`, resp.body[data], { root: true });
          commit(`${module}/SET_ITEM`, resp.body[data], { root: true });
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  remove({ commit, rootState }, { id, route, module, data }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .delete(`${route}/${id}`,{
          headers: {
            'modelName': data,
            'route':route,
            'id': id.toString(),
            'operation': 'archive',
            'from': 'desktop',
          }
        })
        .then(resp => {
          commit(`${module}/UPDATE_ITEM`, resp.body[data], { root: true });
          if (rootState[module][data]) {
            commit(`${module}/SET_ITEM`, resp.body[data], { root: true });
          }
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  unRemove({ commit, rootState }, { id, route, module, data }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .put(`${route}/${id}/reactive`,null,{
          headers: {
            'modelName': data,
            'route':route,
            'id': id.toString(),
            'operation': 'reactive',
            'from': 'desktop',
          }
        })
        .then(resp => {
          commit(`${module}/UPDATE_ITEM`, resp.body[data], { root: true });
          if (rootState[module][data]) {
            commit(`${module}/SET_ITEM`, resp.body[data], { root: true });
          }
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  delete({ commit, rootState }, { id, route, module, data }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .delete(`${route}/${id}/hard`,{
          headers: {
            'modelName': data,
            'route':route,
            'id': id.toString(),
            'operation': 'delete',
            'from': 'desktop',
          }
        })
        .then(resp => {
          if (Object.is(resp.status, 201)) {
            commit(`${module}/UPDATE_ITEM`, resp.body[data], { root: true });
            if (rootState[module][data]) {
              commit(`${module}/SET_ITEM`, resp.body[data], { root: true });
            }
          } else {
            commit(`${module}/DELETE_ITEM`, resp.body[data].id, { root: true });
            if (rootState[module][data]) {
              commit(`${module}/SET_ITEM`, null, { root: true });
            }
          }
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  },

  upload({ commit }, { id, image, route, module, data }) {
    return new Promise(async (resolve, reject) => {
      await Vue.http
        .put(`${route}/${id}/image`, getFormData({ image: image }),{
          headers: {
            'modelName': data,
            'route':route,
            'id': id.toString(),
            'operation': 'upload',
            'from': 'desktop',
          }
        })
        .then(resp => {
          commit(`${module}/UPDATE_ITEM`, resp.body[data], { root: true });
          commit(`${module}/SET_ITEM`, resp.body[data], { root: true });
          resolve(resp.body);
        })
        .catch(err => {
          reject(err.body);
        });
    });
  }
};
