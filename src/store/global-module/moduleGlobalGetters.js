export default {
  getItems: (state, getters, rootState) => (module, property) => {
    return rootState[module][property]
      .slice()
      .sort((firstItem, secondItem) => firstItem.id - secondItem.id);
  },

  getItem: (state, getters, rootState) => (module, property) => {
    return rootState[module][property];
  },
  getNameModel: (state) => (name) => {
    let model = state.models.find(el => el.name == name)
    return model ? model.publicName: name;
  },
  getEntitiesrReport: (state)  => {
    return state.enitities.filter(el => el.report).map(el =>{
      return {
        name: el.name,
        module: el.module,
        property: el.property,
        route: el.route,
        publicName: el.publicName[0].toUpperCase()+''+el.publicName.slice(1),
        report:el.report,
        fields:el.fields,
        columns: el.columns,
      }
    })
  },
  getItemsByDate: (state, getters, rootState) => (module, property, toDate, fromDate) => {
    return rootState[module][property]
      .slice()
      .filter(el => new Date(el.date) <= new Date(toDate) && new Date(el.date) >= new Date(fromDate) )
      .sort((firstItem, secondItem) => firstItem.id - secondItem.id);
  },

  getFieldsModel: (state) => (name) => {
    let fields = state.models.find(el => el.name == name).fields
    return fields ? fields: null;
  },
};
