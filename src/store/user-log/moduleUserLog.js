import state from "./moduleUserLogState";
import actions from "./moduleUserLogActions";
import getters from "./moduleUserLogGetters";
import mutations from "./moduleUserLogMutations";

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
