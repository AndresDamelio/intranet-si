import Vue from "vue";
import App from "./App.vue";

// Vuesax Component Framework
import Vuesax from "vuesax";
import "material-icons/iconfont/material-icons.css"; //Material Icons
import "@fortawesome/fontawesome-free/css/all.css"; //FontAwesome Icons
import "vuesax/dist/vuesax.css"; // Vuesax
Vue.use(Vuesax);

// axios
import axios from "./axios.js";
Vue.prototype.$http = axios;

// vue resources
import VueResource from "vue-resource";

//configuration vue-resource
Vue.use(VueResource);
Vue.http.options.root = process.env.VUE_APP_URL;
Vue.http.interceptors.push((request, next) => {
  request.headers.set(
    "Authorization",
    `Bearer ${window.localStorage.getItem("access_token")}`,
  );
  request.headers.set(
    "mode","cors",
  );
  request.headers.set(
    "credentials","with-credentials"
  );
  next();
});


// mock
import "./fake-db/index.js";

// Theme Configurations
import "../themeConfig.js";

// Globally Registered Components
import "./globalComponents.js";

// Styles: SCSS
import "./assets/scss/main.scss";

// Tailwind
import "@/assets/css/main.css";

// Vue Router
import router from "./router";

// Vuex Store
import store from "./store/store";

// i18n
import i18n from "./i18n/i18n";

// Vuexy Admin Filters
import "./filters/filters";

// Clipboard
import VueClipboard from "vue-clipboard2";
Vue.use(VueClipboard);

// Tour
import VueTour from "vue-tour";
Vue.use(VueTour);
require("vue-tour/dist/vue-tour.css");

// VeeValidate
import VeeValidate from "vee-validate";
import { Validator } from "vee-validate";
import {
  es,
  alpha_number_spaces_rule,
  not_equals_rule,
  smaller_than_rule,
} from "./dictionary.js";

const VueValidationEs = require("vee-validate/dist/locale/es");

Vue.use(VeeValidate, {
  inject: false,
  locale: "es",
  dictionary: {
    es: VueValidationEs
  }
});

Validator.localize(es);
Validator.extend("alpha_number_spaces", alpha_number_spaces_rule);
Validator.extend("not_equals", not_equals_rule, { hasTarget: true });
Validator.extend("smaller_than", smaller_than_rule, { hasTarget: true });


// Vuejs - Vue wrapper for hammerjs
import { VueHammer } from "vue2-hammer";
Vue.use(VueHammer);

// Vue Crontab (Notifications)
import VueCrontab from 'vue-crontab'
Vue.use(VueCrontab)

// PrismJS
import "prismjs";
import "prismjs/themes/prism-tomorrow.css";

// Feather font icon
require("./assets/css/iconfont.css");

// Vue casl
import { abilitiesPlugin} from "@casl/vue";

Vue.use(abilitiesPlugin, store.getters["auth/ability"]);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
