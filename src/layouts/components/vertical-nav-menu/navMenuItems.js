export default [
  {
    url: null,
    name: "Dashboard",
    tag: "1",
    tagColor: "primary",
    icon: "HomeIcon",
    i18n: "Dashboard",
    submenu: [
      {
        url: "/dashboard/resume",
        name: "Dashboard",
        slug: "dashboard-resume",
        i18n: "Dashboard"
      },
      {
        url: "/dashboard/analytics",
        name: "Analítica",
        slug: "dashboard-analytics",
        i18n: "Analítica"
      }
    ]
  },
  {
    header: "Funcionalidades",
    icon: "PackageIcon",
    i18n: "Funcionalidades",
    items: [
      {
        url: "/apps/calendar/vue-simple-calendar",
        name: "Agenda",
        slug: "calendar-simple-calendar",
        icon: "CalendarIcon",
        tagColor: "success",
        i18n: "Agenda"
      },
      {
        url: "/appointments",
        name: "Citas iniciales",
        slug: "appointments",
        icon: "CalendarIcon",
        tagColor: "success",
        i18n: "Citas iniciales"
      },
      {
        url: null,
        name: "Servicios",
        icon: "LayersIcon",
        slug: "user-managment",
        i18n: "Servicios",
        submenu: [
          {
            url: "/services",
            name: "Gestionar servicio",
            slug: "service-managment",
            i18n: "Gestionar servicio"
          },
          {
            url: "/my-services",
            name: "Mis servicios",
            slug: "my-services",
            i18n: "Mis servicios"
          }
        ]
      },
      {
        url: null,
        name: "Actividades",
        icon: "ClipboardIcon",
        i18n: "Actividades",
        submenu: [
          {
            url: "/activities",
            name: "Gestionar actividad",
            slug: "activity-managment",
            i18n: "Gestionar actividad"
          },
          {
            url: "/my-activities",
            name: "Mis actividades",
            slug: "my-activities",
            i18n: "Mis actividades"
          }
        ]
      },
      {
        url: "/users",
        name: "Gestión de usuarios",
        icon: "UsersIcon",
        slug: "user-managment",
        i18n: "Gestión de usuarios"
      },
      {
        url: null,
        name: "Solicitudes",
        icon: "FileTextIcon",
        i18n: "Solicitudes",
        submenu: [
          {
            url: "/request-activities",
            name: "Actividades",
            slug: "request-activities",
            i18n: "Actividades"
          },
          {
            url: "/requests",
            name: "Pacientes",
            slug: "request-patient",
            i18n: "Pacientes"
          },
          {
            url: "/request-services",
            name: "Servicios",
            slug: "request-services",
            i18n: "Servicios"
          }
        ]
      },
      {
        url: "/calendar",
        name: "Agenda",
        slug: "calendar",
        icon: "CalendarIcon",
        tagColor: "success",
        i18n: "Reportes"
      },
      {
        url: "/reports",
        name: "Reportes",
        slug: "reports",
        icon: "ActivityIcon",
        tagColor: "success",
        i18n: "Reportes"
      },
      {
        url: "/user-logs",
        name: "Bitácora",
        slug: "user-log",
        icon: "TabletIcon",
        tagColor: "success",
        i18n: "Bitácora"
      },
      {
        url: null,
        name: "Canal de escucha",
        icon: "RadioIcon",
        i18n: "Canal de escucha",
        submenu: [
          {
            url: "/messages",
            name: "Mensajes",
            slug: "messages",
            i18n: "Mensajes"
          }
        ]
      }
    ]
  },
  {
    header: "Ajustes",
    icon: "SettingsIcon",
    i18n: "Ajustes",
    items: [
      {
        url: null,
        name: "Base de datos",
        slug: "database",
        icon: "DatabaseIcon",
        i18n: "Base de datos",
        submenu: [
          {
            url: "/clean",
            name: "Limpiar",
            slug: "clean",
            i18n: "Limpiar"
          },
          {
            url: "/backup",
            name: "Respaldos",
            slug: "backups",
            i18n: "Respaldos"
          },
          {
            url: "/restore",
            name: "Restaturar",
            slug: "restore",
            i18n: "Restaturar"
          }
        ]
      },
      {
        url: null,
        name: "Configuración",
        icon: "SettingsIcon",
        i18n: "Configuración",
        submenu: [
          {
            url: "/activity-configs",
            name: "Actividades",
            slug: "activities-config",
            i18n: "Actividades"
          },
          {
            url: "/service-configs",
            name: "Servicios",
            slug: "services-config",
            i18n: "Servicios"
          },
          {
            url: "/notification-configs",
            name: "Notificaciones",
            slug: "notifications-config",
            i18n: "Notificaciones"
          }
        ]
      },
      {
        url: null,
        name: "Datos básicos",
        icon: "FilePlusIcon",
        i18n: "Datos básicos",
        submenu: [
          {
            url: "/positions",
            name: "Cargos",
            slug: "position",
            i18n: "Cargos"
          },
          {
            url: "/categories",
            name: "Categorías",
            slug: "category",
            i18n: "Categorías"
          },
          {
            url: "/specialities",
            name: "Especialidades",
            slug: "especiality",
            i18n: "Especialidades"
          },
          {
            url: "/places",
            name: "Lugares",
            slug: "places",
            i18n: "Lugares"
          },
          {
            url: "/reason-rejections",
            name: "Motivos de rechazo",
            slug: "reason-rejection",
            i18n: "Motivos de rechazo"
          },
          {
            url: "/pathologies",
            name: "Patologías",
            slug: "pathology",
            i18n: "Patologías"
          },
          {
            url: "/params",
            name: "Parámetros",
            slug: "params",
            i18n: "Parámetros"
          },
          {
            url: "/questions",
            name: "Preguntas",
            slug: "questions",
            i18n: "Preguntas"
          },
          {
            url: "/resources",
            name: "Recursos",
            slug: "resources",
            i18n: "Recursos"
          },
          {
            url: "/activity-types",
            name: "Tipos de actividad",
            slug: "activity-type",
            i18n: "Tipos de actividad"
          },
          {
            url: "/center-types",
            name: "Tipos de centro",
            slug: "center-type",
            i18n: "Tipos de centro"
          },
          {
            url: "/message-types",
            name: "Tipos de mensaje",
            slug: "message-type",
            i18n: "Tipos de mensaje"
          },
          {
            url: "/notification-types",
            name: "Tipos de notificación",
            slug: "notification-type",
            i18n: "Tipos de notificación"
          },
          {
            url: "/resource-types",
            name: "Tipos de recurso",
            slug: "resource-type",
            i18n: "Tipos de recurso"
          },
          {
            url: "/service-types",
            name: "Tipos de servicio",
            slug: "service-type",
            i18n: "Tipos de servicio"
          },
          {
            url: "/telephone-types",
            name: "Tipos de teléfono",
            slug: "telephone-type",
            i18n: "Tipos de teléfono"
          },
          {
            url: "/schedules",
            name: "Turnos",
            slug: "turn",
            i18n: "Turnos"
          }
        ]
      },
      {
        url: null,
        name: "Organización",
        icon: "CpuIcon",
        i18n: "Organización",
        submenu: [
          {
            url: "/organization/settings",
            name: "Configuración",
            slug: "organization-settings",
            i18n: "Configuración"
          }
        ]
      },

      {
        url: null,
        name: "Seguridad funcional",
        icon: "LockIcon",
        i18n: "Seguridad funcional",
        submenu: [
          {
            url: "/functions",
            name: "Funciones del sistema",
            slug: "functions-system",
            i18n: "Funciones del sistema"
          },
          {
            url: "/roles",
            name: "Roles",
            slug: "roles",
            i18n: "Roles"
          }
        ]
      },
      {
        url: null,
        name: "Web - Móvil",
        icon: "MonitorIcon",
        i18n: "Web - Móvil",
        submenu: [
          {
            url: "/centers",
            name: "Centros",
            slug: "centers",
            i18n: "Centros"
          },
          {
            url: "/mobile-web-configuration",
            name: "Configuración",
            slug: "mobile-web-configuration",
            i18n: "Configuración"
          },
          {
            url: "/faqs",
            name: "Preguntas frecuentes",
            slug: "questions-and-answers",
            i18n: "Preguntas frecuentes"
          },
          {
            url: "/posts",
            name: "Publicaciones",
            slug: "posts",
            i18n: "Publicaciones"
          }
        ]
      },
      {
        url: null,
        name: "Canal de escucha",
        icon: "RadioIcon",
        i18n: "Canal de escucha",
        submenu: [
          {
            url: "/messages",
            name: "Mensajes",
            slug: "messages",
            i18n: "Mensajes"
          }
        ]
      },
    ]
  }
];
